var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var routesRouter = require('./routes/routes');
var aboutUsRouter = require('./routes/about-us');

var app = express();

app.locals.title = 'Voyaqi';

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').__express);

app.use(cors());
app.use(logger('dev'));
app.use(
  express.json({
    type: ['application/json', 'text/plain'],
  })
);
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// i18n Setup
const i18n = require('./lib/i18nConfig')();
app.use(i18n.init);

i18n.setLocale('en');
console.log(i18n.__('Welcome to'));

//so it connects to mongooseError
const mongooseConnection = require('./lib/mongooseConnect');

app.use('/', indexRouter);
app.use('/changeLang', require('./routes/changeLang'));
app.use('/users', usersRouter);
app.use('/routes', routesRouter);
app.use('/about-us', aboutUsRouter);
app.use("/api-doc", require("./routes/api-doc"));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json(err.message);
});

module.exports = app;
