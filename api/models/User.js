'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const nodemailertransport = require('../lib/nodemailerServiceTransport');



const userSchema = mongoose.Schema({
  username: { type: String, unique: true, index: true, required: true },
  password: { type: String, required: true },
  name: { type: String },
  surname: { type: String },
  email: { type: String, unique: true, index: true, required: true },
  city: { type: String, index: true },
  country: { type: String, index: true },
  postalCode: { type: Number, index: true },
  description: { type: String },
  routes: [],
  chatOpens: [],
  passwordResetToken: { type: String },
  passwordResetExpiration: { type: Date },
  likes: { type: Number }
});

userSchema.statics.hashPassword = function (plainPassword) {
  return bcrypt.hash(plainPassword, 10);
};

userSchema.methods.sendEmail = async function (from, subject, body) {

  await nodemailertransport.sendMail({
    from, // sender address
    to: this.email, // list of receivers
    subject, // Subject line
    html: body,
  });

};

const User = mongoose.model('User', userSchema);

module.exports = User;
