## API Methods

### Create a new user

```shell
POST /users/register
```

**Body keys**

- username: String
- email: String
- password: String

### Log in

```shell
POST /users/login
```

**Body keys**

- email: String
- password: String

Success response:

```json
{
  "success": true,
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWY1ZGY0NjliNjJjNjRhNGU1ZjdkYjYiLCJpYXQiOjE1OTMxNzE4MTQsImV4cCI6MTU5MzI1ODIxNH0.KPOwi7E95T8gsKsTqIDCNNb7pTS10Y1hhihzd5Q6SiY"
}
```

### Get users

```shell
GET /users
```

**Get user by username**

```shell
GET /users/:username
```

### Update users

Must provide a valid token

```shell
PUT /users/:id
```

**Body keys**

- token: String
- username: String
- email: String
- password: String

### Deleted User

```shell
POST /users/delete/:id
```

**Body keys**

- token: String


### List of routes

**Get all routes**

Default limit is 10

```shell
GET /routes
```

```json
[
  {
    "from": {
        "coordinates": [-8.72264, 42.23282],
        "_id": "5ef5b803d2155d3f715cd54f",
        "type": "Point"
    },
    "to": {
        "coordinates": [-8.7, 42.2],
        "_id": "5ef5b803d2155d3f715cd550",
        "type": "Point"
    },
    "_id": "5e6bf231992d1b176ff18e33",
    "user_id": "5e6e8e074763f31a1ad53674",
    "**v": 0
  },
  {
    "_id": "5e6959c1b244f42c599d014d",
    "user_id": "ee6e8e074763f31a1ad53675",
    "**v": 0
  }
]
```

**Get a route by its ID**

```shell
GET /routes/:id
```

**Filtering and sorting options**

- user=id --> filter by owner of the routes
- limit=Number --> limit the number of results (10 by default)
- sort

```shell
GET /routes?query=params
```

**Delete a route by its ID**

```shell
DELETE /routes/:id
```

**Update route info**

```shell
DELETE /routes/:id
```

**Find matchings between a given Polygon and the routes**

```shell
 POST routes/find
```
 ##!!IMPORTANT

 The info provided should be sent as a JSON object following the geoJson standard. Example:

 ```json
 {
    "from": {
        "type": "Polygon",
        "coordinates": [[
            [-18, 43],
        [-18, 42],
        [-19, 42],
        [-19, 43],
        [-18, 43] 
      ]]
    },
    "to": {
        "type": "Polygon",
        "coordinates": [[
            [-18, 43],
        [-18, 42],
        [-19, 42],
        [-19, 43],
        [-18, 43] 
      ]]
    }
}
```
In order to try it out using Postman, select body "raw" and JSON on the dropdown menu.

**Create new Route**

```shell
 POST routes/create
```
 ##!!IMPORTANT

 The info provided should be sent as a JSON object following the geoJson standard. Example:

 ```json
     {
        "user_id": "5f01f7eb0ab6e218bec8176c",
        "title": "daily commute"
        "description": "from home to work on labour days"
        "from": {
            "coordinates": [
                -8.74,
                42.24
            ],
            "type": "Point"
        },
        "to": {
            "coordinates": [
                -8.75,
                42.26
            ],
            "type": "Point"
        }
    }
```
In order to try it out using Postman, select body "raw" and JSON on the dropdown menu.

**Send custom JWTAuth**

In order to send customised emails from frontend events to the user, you can use the method `

```shell
 POST routes/mailing/custom
```

It will check if user exists and then sends the email passed on body:

 ```json
     {
        "email":, //user email,
        "message":, //message body, either html or plain text
        "subject": //mail subject
        }
    }
```