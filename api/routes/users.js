'use strict';

const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const jwtAuth = require('../lib/jwtAuth');
const { v4: uuidv4 } = require('uuid');
const service = require('../services/coteRequester');
const mailTemplates = require('../services/mailing/mailTemplates');


const mongoose = require('mongoose');

const User = require('../models/User');

/**
 *  Create a new User
 *  POST /register
 */

router.post('/register', async (req, res, next) => {
  try {
    const email = req.body.email;
    const password = req.body.password;
    const username = req.body.username;

    const usernameExists = await User.findOne({ username });
    const emailExists = await User.findOne({ email });

    console.log(usernameExists);

    if (!email || !password || !username) {
      const error = new Error('No user or password provided');
      error.status = 401;
      res.status(401).json({
        success: false,
        error: error.message,
        description:
          "Your request must provide a body with a 'user' an 'email' and a 'password' key",
      });
      return;
    }

    if (usernameExists) {
      const error = new Error('Username already in use');
      error.status = 401;
      res.status(401).json({
        success: false,
        error: error.message,
        description: `The username ${username} is already taken, try another one`,
      });
      return;
    }

    if (emailExists) {
      const error = new Error('Email already in use');
      error.status = 401;
      res.json({
        success: false,
        error: error.message,
        description: `The email ${email} already has an account`,
      });
      return;
    }

    const userData = {
      username,
      email,
      password: await User.hashPassword(password),
    };

    const user = new User(userData);

    const createUser = await user.save();

    const template = mailTemplates[0];

    const mailOptions = {
      from: process.env.ADMIN_EMAIL,
      to: user.email,
      subject: `${template.subject}, ${user.username}`,
      body: template.text,
    };

    service.sendMail(mailOptions);
    
    res.status(201).json({ success: true, user: createUser });

  } catch (error) {
    next(error);
  }
});

/**
 *  Log in
 *  POST /login
 */

router.post('/login', async (req, res, next) => {
  try {
    const email = req.body.email;
    const password = req.body.password;
    const remember = req.body.remember;

    const user = await User.findOne({ email });

    if (!email || !password) {
      const error = new Error('No user or password provided');
      error.status = 401;
      res.status(401).json({
        success: false,
        error: error.message,
        description: "Your request must provide a body with an 'email' and a 'password' key",
      });
      return;
    }

    if (!user || !(await bcrypt.compare(password, user.password))) {
      const error = new Error('Invalid credentials');
      error.status = 401;
      res.status(401).json({
        success: false,
        error: error.message,
        description: 'your email or password are incorrect',
      });
      return;
    }

    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, {
      expiresIn: remember ? '1m' : '1d',
    });

    res.json({ success: true, token, user: user });
  } catch (error) {
    next(error);
  }
});

/**
 *  Update a user
 *  PUT /users/:id
 */
router.put('/:id', jwtAuth(), async (req, res, next) => {
  try {
    const _id = req.params.id;

    const userData = { ...req.body };

    const saveUser = await User.findOneAndUpdate({ _id: _id }, { ...userData }, {
      new: true,
      useFindAndModify: false,
    });
    

    res.json({ success: true, result: saveUser });
  } catch (err) {
    next(err);
  }
});

/**
 *  Deleted an user
 *  DELETE /users/delete/:id
 */
router.delete('/delete/:id', jwtAuth(), async (req, res, next) => {
  try {
    const id = req.params.id;
    const deletedUser = await User.findByIdAndDelete(id);

    deletedUser
      ? res.status(200).json({ success: true, deletedUser })
      : res.status(200).json({
        success: false,
        error: 'The user could not be deleted',
        description: 'The request has been processed but the user could not be deleted',
      });

  } catch (err) {
    next(err);
  }
});

/**
 *  Reset password request
 *  POST /users/reset
 */

router.post('/reset', async (req, res, next) => {
  const email = req.body.email;

  const user = await User.findOne({ email });

  if (!user) {
    const error = new Error('No user found with provided email');
    error.status = 401;
    res.status(401).json({
      success: false,
      error: error.message,
      description: "email not found"
    });
    return;
  }

  const passwordResetToken = uuidv4();
  const passwordResetExpiration = Date.now() + 30 * 60 * 1000; // 30min

  try {
    await user.updateOne({ passwordResetToken, passwordResetExpiration });

    const hashedResetPassword = await User.hashPassword(passwordResetToken);
    console.log(hashedResetPassword);

    const mailOptions = {
      from: process.env.ADMIN_EMAIL,
      to: user.email,
      subject: 'Reset your account password',
      body: `
            <h2>Reset password</h2>
            <p>To reset your password click the following link:</p>
            <p><a clicktracking=off href="${process.env.URL}/reset/confirmpassword?user=${user.username}&token=${hashedResetPassword}"> Create new password </a></p>`,
    };

    service.sendMail(mailOptions);

    res.json({
      success: true,
      message: `email sent to ${email}`,
    });
  } catch (error) {
    next(error);
  }
});

/**
 *  Reset password approve
 *  GET /users/reset
 */

router.get('/reset', async (req, res, next) => {
  const username = req.query.user;
  const hashedResetPassword = req.query.token;

  console.log("on router.get", username);

  if (!username || !hashedResetPassword) {
    const error = new Error('Invalid url');
    error.status = 401;
    res.status(401).json({
      success: false,
      error: error.message,
      description: "the data provided doesn't match, please reset your password again"

    });
    return;
  }

  const user = await User.findOne({ username });

  if (!user) {
    const error = new Error('Invalid user');
    error.status = 401;
    res.status(401).json({
      success: false,
      error: error.message,
      description: "your user can't be found. Please check you have registered properly or try again to reset the password"
    });
    return;
  }

  if (
    !(await bcrypt.compare(user.passwordResetToken, hashedResetPassword)) ||
    Date.now() > user.passwordResetExpiration
  ) {
    const error = new Error('Invalid token');
    error.status = 401;
    res.status(401).json({
      success: false,
      error: error.message,
      description: "the temporary password has timed out, please reset your password again"
    });
    return;
  }

  const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, {
    expiresIn: '1d',
  });

  res.json({
    success: true,
    token,
    id: user._id
  });
});

/**
 *  Update password after reset
 *  PUT /reset/:id
 */
router.put('/reset/:id', jwtAuth(), async (req, res, next) => {
  try {
    const _id = req.params.id;
    const password = await User.hashPassword(req.body.password);

    const saveUser = await User.findOneAndUpdate({ _id: _id }, { password }, {
      new: true,
      useFindAndModify: false,
    });

    res.json({ success: true, result: saveUser });
  } catch (err) {
    next(err);
  }
});

/* GET user by username */
router.get('/:username', async (req, res, next) => {
  try {
    const username = req.params.username;
    const user = await User.findOne({ username });

    if (!user) {
      const err = new Error('not found');
      err.status = 404;
      next(err);
      return;
    }

    res.json({ result: user });
  } catch (err) {
    next(err);
  }
});

/* GET user by user ID */
router.get('/id/:id', async (req, res, next) => {
  try {
    const _id = req.params.id;
    const user = await User.findOne({ _id });

    if (!user) {
      const err = new Error('not found');
      err.status = 404;
      next(err);
      return;
    }

    res.json({ result: user });
  } catch (err) {
    next(err);
  }
});

/* GET users listing. */
router.get('/', async (req, res, next) => {
  res.send(await User.find()); // ¿devolver users con sus passwords (ya se, hasheados) y todos los datos lo veo... pericoloso? -> si tuviéramos al superadmin, este sería su método jijiji.
});

/**Update routes array in user model 
 * PUT /users/add/routes
*/
router.put('/add/routes', jwtAuth(), async (req, res, next) => {
  try {
    
    const filter = { _id: req.body.user_id };
    const update = { $push: { routes: req.body.route_id } };

    let doc = await User.findOneAndUpdate(filter, update, {
      new: true,
      useFindAndModify: false
    });
    doc._id; 
    doc.routes;

    res.json({ success: true });
  } catch (err) {
    next(err);
  }
});

router.post('/mailing/custom', jwtAuth(), async (req, res, next) => {
  
  try {
    const messageBody = req.body.message;
    const messageSubject = req.body.subject;
    const email = req.body.email;
    console.log(req.body)
    const user = await User.findOne({email});
  
    if (!user) {
      const error = new Error('email not valid');
      error.status = 401;
      res.status(401).json({
        success: false,
        error: error.message,
        description:
          "the email provided is not registered on the platform",
      });
      return;
    }

    const mailOptions = {
      from: process.env.ADMIN_EMAIL,
      to: user.email,
      subject: messageSubject,
      body: messageBody
    };
    
    service.sendMail(mailOptions);
  
    res.json({success: true, delivered_to: user.email, at: new Date().getTime()})

  } catch (err) {
    next(err);
  }
});


/**Update routes array in user model 
 * PUT /users/remove/routes
*/
router.put('/remove/routes', jwtAuth(), async (req, res, next) => {
  try {
    
    const filter = { _id: req.body.user_id };
    const update = { $pull: { routes: req.body.route_id } };

    let doc = await User.findOneAndUpdate(filter, update, {
      new: true,
      useFindAndModify: false
    });
    doc._id; 
    doc.routes;

    res.json({ success: true });
  } catch (err) {
    next(err);
  }
});

/**
 * Update likes in user model
 * PUT /users/like/:id
 */
router.put('/like/:id', async (req, res, next) => {
  try{

    const filter = { _id: req.body.user_id };
    const update = { $inc: { likes: 1 } };

    let doc = await User.findOneAndUpdate(filter, update, {
      new: true,
      useFindAndModify: false
    });
    doc._id; 
    doc.likes;

    res.json({ success: true, result: doc.likes });
  }catch(err){
    next(err);
  }
});


module.exports = router;
