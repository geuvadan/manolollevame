
/**
 * @swagger
 * /users/register:
 *  post:
 *      tags:
 *          - "users"
 *      summary: Create User
 *      description: Use this request to create a new user, this operation will register an user
 *      produces:
 *         - application/json
 *      parameters:
 *         - in: body
 *           name: user info
 *           description: The user info to create
 *           schema:
 *               $ref: '#/definitions/UserCreate'
 *      responses:
 *       201:
 *         description: Created!
 */

/**
 * @swagger
 * /users/login:
 *  post:
 *      tags:
 *          - "users"
 *      summary: Login User
 *      description: Use this request to login in app, this operation will login and retrieves a token. You must previously register in the app or request it from the administrators
 *      produces:
 *         - application/json
 *      parameters:
 *         - in: body
 *           name: user info
 *           description: The user info to create
 *           schema:
 *               $ref: '#/definitions/UserLogin'
 *      responses:
 *       201:
 *         description: Login!
 */

/**
 * @swagger
 * /users/{id}:
 *  put:
 *      tags:
 *          - "users"
 *      summary: Update user info
 *      description: Use to request to update user info
 *      produces:
 *         - application/json
 *      parameters:
 *         - in: header
 *           name: token
 *           description: Token API after login user
 *         - in: path
 *           name: id
 *           description: User id
 *         - in: body
 *           name: user info
 *           description: The user info to create
 *           schema:
 *               $ref: '#/definitions/UserUpdate'
 *      responses:
 *       200:
 *         description: Advertisements
 *         schema:
 *         type: json
 */

/**
 * @swagger
 * /users/{username}:
 *  get:
 *      tags:
 *          - "users"
 *      summary: Get User by username
 *      description: This request returns an user by username. 
 *      produces:
 *         - application/json
 *      parameters:
 *         - in: path
 *           name: username
 *           description: User username unique
 *      responses:
 *       200:
 *         description: Username by username
 *         schema:
 *         type: json
 */

 /**
 * @swagger
 * definitions:
 *  UserCreate:
 *      type: object
 *      properties:
 *          email:
 *              type: string
 *          password:
 *              type: string
 *          username:
 *              type: string
 * 
 *  UserLogin:
 *      type: object
 *      properties:
 *          email:
 *              type: string
 *          password:
 *              type: string
 * 
 *  UserUpdate:
 *      type: object
 *      properties:
 *          email:
 *              type: string
 *          name:
 *              type: string
 *          surname:
 *              type: string
 *          city:
 *              type: string
 *          country:
 *              type: string
 *          postalCode:
 *              type: number
 *          description:
 *              type: string
 * 
 */