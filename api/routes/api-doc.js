"use strict";

const express = require("express");
const app = express();

const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");


//Swagger documentation
const swaggerOptions = {
    swaggerDefinition:{
        info:{
            version: "1.0.0",
            title: "Voyaqi API",
            description: "Voyaqi API Documentation for use",
            contact:{
                name: "Team Malleme",
                url: "https://www.voyaqi.app/about-us",
            },
            servers: [
                {
                    url:"https://www.voyaqi.app"
                }
            ],
            
        },
    },
    apis: ["./routes/users-doc.js", "./routes/routes-doc.js"],
    tags: [
        {
            name: "users",
            description: "api methods for users"
        },
        {
            name: "routes",
            description: "api methods for routes"
        }
    ]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/", swaggerUI.serve, swaggerUI.setup(swaggerDocs));


module.exports = app;