
/**
 * @swagger
 * /routes:
 *  get:
 *      tags:
 *          - "routes"
 *      summary: Get all routes
 *      description: This request returns a json with routes list. 
 *      produces:
 *         - application/json
 *      responses:
 *       200:
 *         description: Get all routes saved in application
 *         schema:
 *         type: json
 */

/**
 * @swagger
 * /routes/create:
 *  post:
 *      tags:
 *          - "routes"
 *      summary: Create Route
 *      description: Use this request to create a new route
 *      produces:
 *         - application/json
 *      parameters:
 *         - in: body
 *           name: route info
 *           description: The route info to create
 *           schema:
 *               $ref: '#/definitions/Route'
 *      responses:
 *       201:
 *         description: Created!
 */

/**
 * @swagger
 * /routes/{id}:
 *  get:
 *      tags:
 *          - "routes"
 *      summary: Get Route by Id
 *      description: This request returns a route by id. 
 *      produces:
 *         - application/json
 *      parameters:
 *         - in: path
 *           name: id
 *           description: Route id unique
 *      responses:
 *       200:
 *         description: result route by Id
 *         schema:
 *         type: json
 */

/**
 * @swagger
 * /routes/{id}:
 *  put:
 *      tags:
 *          - "routes"
 *      summary: Create Route
 *      description: Use this request to create a new route
 *      produces:
 *         - application/json
 *      parameters:
 *         - in: path
 *           name: id
 *           description: Route id unique
 *         - in: body
 *           name: route info
 *           description: The route info to create
 *           schema:
 *               $ref: '#/definitions/Route'
 *      responses:
 *       201:
 *         description: Updated!
 */


/**
 * @swagger
 * /routes/{id}:
 *  delete:
 *      tags:
 *          - "routes"
 *      summary: Delete Route by Id
 *      description: This request deletes a route by id. 
 *      produces:
 *         - application/json
 *      parameters:
 *         - in: path
 *           name: id
 *           description: Route id unique
 *      responses:
 *       200:
 *         description: result delete route by Id
 */

 /**
 * @swagger
 * definitions:
 *  Route:
 *      type: object
 *      properties:
 *          user_id:
 *              type: string
 *          city:
 *              type: string
 *          title:
 *              type: string
 *          description:
 *              type: string
 *          transport:
 *              type: string
 *          seats:
 *              type: number
 *          departure:
 *              type: string
 *          from:
 *              type: object
 *              properties:
 *                  coordinates:
 *                      type: array
 *                      items:
 *                          type: number
 *                  type:
 *                      type: string
 *                      enum: Point                      
 *          to:
 *              type: object
 *              properties:
 *                  coordinates:
 *                      type: array
 *                      items:
 *                          type: number
 *                  type:
 *                      type: string
 *                      enum: Point  
 */