require('dotenv').config();

const templates = [
    {
        id: 0,
        title: "register mail",
        subject: `thanks for registering at ${process.env.URL_MAILING_ALIAS}`,
        text: `<h1> KUDOS! you're already registered at ${process.env.URL_MAILING_ALIAS}</h1> 
                    <p> Now, what? </p> 
                <div>
                    <ul style="list-style-type:none;">
                        <li>You can start sharing your routes directly on our <a href='https://www.suite.voyaqi.app'>app</a></li>
                        <li>You can go check out other user's routes</li>
                        <li> You can complete your <a href="https://www.suite.voyaqi.app/profile/user">profile </a> to help other users get to know you better</li>
                        <li>And you can spread the word, telling for family and friends about the awesome community you have just join!</li>
                    </ul>
                    <p> And remember, you can always sing out by deleting your account at your profile section in our app</p>
                
                </div>`
    },
    {
        subject: `we're sorry to watching you go`,
        text: `<h1> Bye, bye, our firend </h1> 
                <p> It is sad to watching you go, and we hope to have you join us again very soon</p> 
                <div>
                    <p> Meanwhile we would trully appreciated it if you spare us a minute to let us know why you've decided to delete your account...</p>
                    <ul style="list-style-type:none;">
                        <li>Was it difficult to use? why?</li>
                        <li>weren't there enough routes and users and you decided to let it go? </li>
                        <li> Other reasons?</li>
                    </ul>
                    <p> Thanks a lot for your help. We want to improve the service and your opinion <strong>means the world to us</strong></p>

                    <h3>Voyaqi team</h3>
                </div>`
    }
]

module.exports = templates;