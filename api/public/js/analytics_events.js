const aboutUsButton = document.querySelector('#about-us-footer-button'); 
const navbarLogin = document.querySelector('.navbar-login'); 
const navbarRegister = document.querySelector('.navbar-register'); 
const goToAppHomeButton = document.querySelector('#go-to-app-button');
const personalLinks = document.querySelectorAll('.personal-link');
const indexLinks = document.querySelectorAll('.index-link')

personalLinks.forEach(el => el.addEventListener("click", (evt)=> {
    gtag('event', 'click', {
        'event_category' : 'team link',
        'event_label' : `${el.id}`
      });;
}));

indexLinks.forEach(el => el.addEventListener("click", (evt)=> {
    gtag('event', 'click', {
        'event_category' : 'go to app',
        'event_label' : `${el.id}`
      });;
}));

goToAppHomeButton.addEventListener("click", (evt)=> {

    gtag('event', 'click', {
        'event_category' : 'go to app',
        'event_label' : 'home main link'
      });;
});

aboutUsButton.addEventListener("click", (evt)=> {

    gtag('event', 'click', {
        'event_category' : 'about-us link',
        'event_label' : 'about-us'
      });;
});

navbarLogin.addEventListener("click", (evt)=> {
    gtag('event', 'click', {
        'event_category' : 'to login',
        'event_label' : 'navbar link'
      });;
});

navbarRegister.addEventListener("click", (evt)=> {
    gtag('event', 'click', {
        'event_category' : 'to register',
        'event_label' : 'navbar link'
      });;
});







