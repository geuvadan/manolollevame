const msgs = require('./msgsChat');
const route = require('./routeChat');;
const usersChat = require('./usersChat')

module.exports = function (io) {
  let users = {};


  io.on('connection', async socket => {
    console.log('new user connected');

    //**** NEW CONNECTION *******

    socket.on('new connect', async (data, cb) => {

      console.log('connection');

      socket.username = data.username;
      socket.routeId = data.routeId;
      // store the user and title owner route in socket
      const dataRoute = await route.data(data.routeId);
      socket.userOwnerRoute = dataRoute.userOwnerRoute;
      socket.titleRoute = dataRoute.titleRoute;
      // send client to room 
      socket.join(data.routeId);
      //update object usernames, each user has the object socket 
      users[socket.username] = socket;


      // get messages and emit
      //I am not the router owner
      if (socket.userOwnerRoute !== socket.username) {
        let messages = await msgs.notOwner(socket.routeId, socket.username)

        users[socket.username].emit('messages', {
          messages,
          usersChat: [socket.userOwnerRoute],
          isOwner: false
        })
      } // I am not the router owner

      else {
        let messages = await msgs.owner(socket.routeId)
        let usersRoom = await usersChat.find(messages, socket.userOwnerRoute)

        users[socket.username].emit('messages', {
          messages,
          usersChat: usersRoom,
          isOwner: true
        });
      }
    });

    //**** SEND MESSAGE *******
    socket.on('send message', async (data, cb) => {

      //save new msg in DB
      msgs.sendMsg(
        socket.username,
        socket.userOwnerRoute,
        socket.routeId,
        data.msg,
        data.userReceiver);

      //I am the router owner
      if (socket.userOwnerRoute === socket.username) {
        let messagesOwner = await msgs.owner(socket.routeId)
        let messagesNotOwner = await msgs.notOwner(socket.routeId, data.userReceiver)
        let usersRoom = await usersChat.find(messagesOwner, socket.userOwnerRoute)

        users[socket.username].emit('message saved', {
          msgs: messagesOwner,
          isOwner: true,
          userSelected: data.userReceiver,
          usersChat: usersRoom
        })
        //receiver is connect and the same room (routeId)
        if (users[data.userReceiver] &&
          users[data.userReceiver].routeId === socket.routeId) {
          users[data.userReceiver].emit('message saved', {
            msgs: messagesNotOwner,
            isOwner: false,
            userSelected: socket.userOwnerRoute
          })
        }
      } else { // I am NOT the router owner
        let messagesOwner = await msgs.owner(socket.routeId)
        let messagesNotOwner = await msgs.notOwner(socket.routeId, socket.username)
        let usersRoom = await usersChat.find(messagesOwner, socket.userOwnerRoute)

        //if exist messages add ooenChat in bothUser
        if (messagesNotOwner.length > 0) {
          usersChat.openChat(socket.username, data.userReceiver, socket.routeId, socket.titleRoute)
        }

        users[socket.username].emit('message saved', {
          msgs: messagesNotOwner,
          isOwner: false,
          userSelected: socket.userOwnerRoute
        })

        if (users[data.userReceiver] &&
          users[data.userReceiver].routeId === socket.routeId) {
          users[data.userReceiver].emit('message saved', {
            msgs: messagesOwner,
            isOwner: true,
            userSelected: socket.username,
            usersChat: usersRoom
          })
        }
      }
    });

    socket.on('disconnectUser', async (data) => {
      delete users[data.username]
    })
  });
};





