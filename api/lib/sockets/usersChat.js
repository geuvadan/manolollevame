const User = require('../../models/User');


module.exports = usersChat = {
  find: async (messages, owner) => {
    const users = messages.reduce((result, current) => {
      return result.includes(current.username) ? result : [...result, current.username]
    }, [])

    const allUsersExceptOwner = users.filter(user => user !== owner)
    return allUsersExceptOwner
  },
  openChat: async (user, owner, routeId, titleRoute) => {
    const userNotOwner = await User.findOne({ username: user })
    const userOwner = await User.findOne({ username: owner })


    const countChatsNotOwner = userNotOwner.chatOpens.filter(chat => {
      return chat.routeId === routeId
    })

    if (countChatsNotOwner.length === 0) {
      userNotOwner.chatOpens.push({ routeId, titleRoute })
      userNotOwner.save()
    }

    const countChatsOwner = userOwner.chatOpens.filter(chat => {
      return chat.routeId === routeId
    })


    if (countChatsOwner.length === 0) {
      userOwner.chatOpens.push({ routeId, titleRoute })
      userOwner.save()
    }
  }
}
