# Project MANOLO LLÉVAME!


Para iniciar el proyecto, clona el repo. ;P

## Carpeta /client

Ve a la carpeta /client, instala las dependencias e inicia la aplicación. Si todo ha ido bien, debería correr en el puerto 3000.

cd client

npm install

npm start

## Carpeta /api

Ve a la carpeta /api, instala las dependencias e inicia la aplicación. Si todo ha ido bien, debería correr en el puerto 9000.

cd api

npm install

npm start


*Este es un inicio para el proyecto, he añadido axios para hacer una prueba de conexión /client <> /api y CORS en el proyecto de express*

*Pendiente por añadir los scripts de inicialización en modo desarrollo en la carpeta /api, añadir un linter, configurar un prettier para tener el código más homogéneo posible*
