export default {
  setUser: user => {
    localStorage.setItem('userName', JSON.stringify(user));
  },

  getUser: () => {
    const user = localStorage.getItem('userName');
    return JSON.parse(user);
  }
}