import {useState, useEffect} from 'react';

export const usePosition = () => {
  const [position, setPosition] = useState({});
  const [error, setError] = useState(null);
  
 /* const onChange = ({coords}) => {
    setPosition({
      latitude: coords.latitude,
      longitude: coords.longitude,
    });
  };*/
  const onError = (error) => {
    setError(error.message);
  };

  const success = pos => {
    setPosition({
        userLat: pos.coords.latitude,
        userLng: pos.coords.longitude,
      });
    localStorage.setItem("last location", JSON.stringify({ 
        lat: pos.coords.latitude,
        lng: pos.coords.longitude,
    }))
  }

  const options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  useEffect(() => {
    const geo = navigator.geolocation;
    if (!geo) {
      setError('Geolocation is not supported');
      return;
    }
    geo.getCurrentPosition(success, onError, options)
   /*const watcher = geo.watchPosition(onChange, onError);
    return () => geo.clearWatch(watcher);*/
  }, []);

  return {...position, error};
}