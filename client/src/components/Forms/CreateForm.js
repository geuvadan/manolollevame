import React from "react";

import { withTranslation } from 'react-i18next';

import { FormGroup, Label, Input, Button, Container, Row, Col, Card, CardBody, Form } from "reactstrap";

import "../../assets/css/styles.css";

function CreateForm({ handleCoordinates, handleInputChange, handleSave, data, address, ...props }) {

  const { t } = props;

  return (
      <Container className="mt--4" fluid>
        <Row className="">
          <Col className="" xl="10">
            <Card className="bg-secondary shadow">
              <CardBody className="pt-0">

                <Form onSubmit={(event) => {
                  event.preventDefault();
                }}
                >
                  <h6 className="heading-small text-muted mb-4">Punto de partida y llegada</h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <Label for="cityRoute">
                            {t('SearchRouteForm.create-route-departure-city')}
                          </Label>
                          <Input
                            type="text"
                            name="city"
                            id="cityRoute"
                            placeholder={t(
                              'SearchRouteForm.create-route-placeholder-departure-city'
                            )}
                            onChange={handleInputChange}
                            value={data.city}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <Label for="fromRoute">
                            {t('SearchRouteForm.create-route-departure')}
                          </Label>
                          <Input
                            type="text"
                            name="from"
                            id="fromRoute"
                            placeholder={t(
                              'SearchRouteForm.create-route-placeholder-departure'
                            )}
                            onBlur={handleCoordinates}
                            onChange={handleInputChange}
                            value={address.from}
                            required
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="6">
                        <FormGroup>
                          <Label for="toRoute">
                            {t('SearchRouteForm.create-route-arrival')}
                          </Label>
                          <Input
                            type="text"
                            name="to"
                            id="toRoute"
                            placeholder={t('SearchRouteForm.create-route-placeholder-arrival')}
                            onBlur={handleCoordinates}
                            onChange={handleInputChange}
                            value={address.to}
                            required
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />
                  <h6 className="heading-small text-muted mb-4">
                    {t('SearchRouteForm.create-route-nameAndDetail')}{' '}
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <Label for="titleRoute">
                            {t('SearchRouteForm.create-route-name')}
                          </Label>
                          <Input
                            type="text"
                            name="title"
                            id="titleRoute"
                            placeholder={t('SearchRouteForm.create-route-placeholder-name')}
                            onChange={handleInputChange}
                            value={data.title}
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="6">
                        <FormGroup>
                          <Label for="descriptionRoute">
                            {t('SearchRouteForm.create-route-description')}{' '}
                          </Label>
                          <Input
                            type="textarea"
                            name="description"
                            id="descriptionRoute"
                            placeholder={t(
                              'SearchRouteForm.create-route-placeholder-description'
                            )}
                            onChange={handleInputChange}
                            value={data.description}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>

                  <hr className="my-4" />
                  <h6 className="heading-small text-muted mb-4">
                    {t('SearchRouteForm.create-route-more-info')}{' '}
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="4">
                        <FormGroup>
                          <Label for="transportRoute">
                            {t('SearchRouteForm.create-route-transport')}{' '}
                          </Label>
                          <Input
                            type="select"
                            name="transport"
                            id="transportRoute"
                            placeholder={t(
                              'SearchRouteForm.create-route-placeholder-transport'
                            )}
                            onChange={handleInputChange}
                            value={data.transport}
                          >
                            <option>{t('SearchRouteForm.create-route-select-one')} </option>
                            <option>
                              {t('SearchRouteForm.create-route-transportOption-car')}{' '}
                            </option>
                            <option>
                              {t('SearchRouteForm.create-route-transportOption-motorbike')}{' '}
                            </option>
                            <option>
                              {t('SearchRouteForm.create-route-transportOption-bike')}{' '}
                            </option>
                            <option>
                              {t('SearchRouteForm.create-route-transportOption-horse')}{' '}
                            </option>
                            <option>
                              {t('SearchRouteForm.create-route-transportOption-bus')}{' '}
                            </option>
                          </Input>
                        </FormGroup>
                      </Col>

                      <Col lg="4">
                        <FormGroup>
                          <Label for="seatsRoute">
                            {t('SearchRouteForm.create-route-seats')}
                          </Label>
                          <Input
                            type="select"
                            name="seats"
                            id="seatsRoute"
                            placeholder={t('SearchRouteForm.create-route-placeholder-seats')}
                            onChange={handleInputChange}
                            value={data.seats}
                            required
                          >
                            <option>{t('SearchRouteForm.create-route-select-one')} </option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                          </Input>
                        </FormGroup>
                      </Col>

                      <Col lg="4">
                        <FormGroup>
                          <Label for="departureRoute">
                            {t('SearchRouteForm.create-route-departure-time')}
                          </Label>
                          <Input
                            type="time"
                            name="schedule"
                            id="departureRoute"
                            onChange={handleInputChange}
                            value={data.schedule}
                            required
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>

                  <div className="pl-lg-4 mt-4">
                    <Button color="primary" className="save-button-createRoute w-100" onClick={handleSave}>
                      {' '}
                      {t('SearchRouteForm.save-routes-btn')}{' '}
                    </Button>
                  </div>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
  )
}
export default withTranslation('translations')(CreateForm)