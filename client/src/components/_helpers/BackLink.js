import React from "react";
import { Link, useHistory } from "react-router-dom";

export default function ButtonBack({ children }) {
  const history = useHistory();
  return (
    <Link onClick={() => history.goBack()}>
      {children}
    </Link>
  );
}
