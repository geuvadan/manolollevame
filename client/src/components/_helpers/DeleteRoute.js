/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import API from '../../services/API';


import { withTranslation } from 'react-i18next';

import { useHistory } from 'react-router-dom';


const DeleteRoute = (props) => {
  const { buttonLabel, className, size, routeId, userId } = props;

  const { t } = props;
  const lang = localStorage.getItem('i18nextLng');

  const [modal, setModal] = useState(false);
  const [statusButton, setStatusButton] = useState(false);
  const [message, setMessage] = useState(
    lang === 'en'
      ? 'You can delete the route, but keep in mind that some travelers will be sad'
      : 'Puedes eliminar la ruta, pero recuerda que otros viajeros se pondrán tristes'
  );

  const history = useHistory();

  const toggle = () => setModal(!modal);

  const onHandleRemove = async () => {
    try {
      const routeUserData = {
        user_id: userId,
        route_id: routeId,
      };


      await API.deleteRoute(routeId)
        .then(API.updateRoutesInUser(routeUserData, 'remove'))
        .then(setMessage(lang === 'en' ? 'The route has been removed' : 'La ruta ha sido borrada'))
        .then(setStatusButton(true));

      setTimeout(() => {
        history.push('/admin/index');
        window.location.reload();
      }, 2500);
    } catch (err) {
      setMessage(
        lang === 'en' ? 'Oops, error! Try it later' : '¡Oh! ha habido un error, intentalo más tarde'
      );
    }
  };


  return (
    <div>
      <Button color="danger" onClick={toggle} size={size}>
        {buttonLabel}
      </Button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>{t('DeleteRoute.sure')} </ModalHeader>
        <ModalBody>{message}</ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={onHandleRemove} disabled={statusButton}>
            {t('DeleteRoute.confirm_btn')}
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            {t('DeleteRoute.cancel_btn')}
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default withTranslation('translations')(DeleteRoute);
