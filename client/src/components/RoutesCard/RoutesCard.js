import React from 'react';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Row } from 'reactstrap';

import CardMap from '../Maps/CardMap';
import DeleteRoute from '../_helpers/DeleteRoute';

import { withTranslation } from 'react-i18next';

function RoutesCard({ title, from, to, city, _id, user_id, remove, edit, ...props }) {
  const { t } = props;

  let removeButton;
  let editOrViewButton;

  if (remove) {
    removeButton = (
      <DeleteRoute
        buttonLabel={t('RoutesCard.delete-route')}
        size="sm"
        className="mt-5"
        routeId={_id}
        userId={user_id}
      />
    );
  }

  if (edit) {
    editOrViewButton = <Link to={`/route/edit/${_id}`}>{t('RoutesCard.edit-route')}</Link>;
  } else {
    editOrViewButton = <Link to={`/route/${_id}`}>{t('RoutesCard.view-route')}</Link>;
  }

  return (
    <>
      <div>
        <Card className="shadow mb-5">
          <CardHeader className="bg-transparent">
            <Row className="align-items-center">
              <div className="col">
                <h2>{title}</h2>
                <h5 className="text-uppercase text-muted ls-1 mb-1">{city}</h5>
              </div>
            </Row>
          </CardHeader>
          <CardMap
            from={{ lat: from.coordinates[1], lng: from.coordinates[0] }}
            to={{ lat: to.coordinates[1], lng: to.coordinates[0] }}
          />
          <CardBody className="d-flex justify-content-between">
            {editOrViewButton}
            {removeButton}
          </CardBody>
        </Card>
      </div>
    </>
  );
}

export default withTranslation('translations')(RoutesCard);

