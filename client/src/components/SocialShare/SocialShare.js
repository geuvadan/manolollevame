import React from 'react';
import "./style.css";

import { withTranslation } from 'react-i18next';
import {Helmet} from "react-helmet";

function SocialShare({id, ...props}) {

  const { t } = props;

    (function() {
        function initSocialShare(button) {
          button.addEventListener('click', function(event){
            event.preventDefault();
            var social = button.getAttribute('data-social');
            var url = getSocialUrl(button, social);
            (social === 'mail')
              ? window.location.href = url
              : window.open(url, social+'-share-dialog', 'width=626,height=436');
          });
        };
      
        function getSocialUrl(button, social) {
          var params = getSocialParams(social);
          var newUrl = '';
          for(var i = 0; i < params.length; i++) {
            var paramValue = button.getAttribute('data-'+params[i]);
            if(params[i] === 'hashtags') paramValue = encodeURI(paramValue.replace(/#| /g, ''));
            if(paramValue) {
              (social === 'facebook') 
                ? newUrl = newUrl + 'u='+encodeURIComponent(paramValue)+'&'
                : newUrl = newUrl + params[i]+'='+encodeURIComponent(paramValue)+'&';
            }
          }
          if(social === 'linkedin') newUrl = 'mini=true&'+newUrl;
          return button.getAttribute('href')+'?'+newUrl;
        };
      
        function getSocialParams(social) {
          var params = [];
          switch (social) {
            case 'twitter':
              params = ['text', 'hashtags'];
              break;
            case 'facebook':
            case 'linkedin':
              params = ['text', 'url'];
              break;
            case 'whatsapp' :
              params = ['text','url'];
              break;
            case 'mail':
              params = ['subject', 'body'];
              break;
          }
          return params;
        };
      
        var socialShare = document.getElementsByClassName('js-social-share');
        if(socialShare.length > 0) {
          for( var i = 0; i < socialShare.length; i++) {
            (function(i){initSocialShare(socialShare[i])})(i);
          }
        }
      }());

      const messageInit = t('SocialShare.message');
      const url = `https://www.suite.voyaqi.app/route/${id}`;
      const message = `${messageInit}: ${url}`;
      const subject = t('SocialShare.subject');

    return (
        <section className="">

            <ul className="sharebar">
                <li>
                    <a className="sharebar__btn js-social-share" data-social="twitter" data-text={message} data-hashtags="#carsharing, #share, #ecology" href="https://twitter.com/intent/tweet">
                        <i className="fab fa-twitter"></i>
                    </a>
                </li>

                <li>
                    <a className="sharebar__btn js-social-share" data-social="facebook" data-url={url} href="http://www.facebook.com/sharer.php">
                        <i className="fab fa-facebook"></i>
                    </a>
                </li>

                <li>
                    <a className="sharebar__btn js-social-share" data-social="linkedin" data-url={url} href="https://www.linkedin.com/shareArticle">
                        <i className="fab fa-linkedin"></i>
                    </a>
                </li>

                <li>
                    <a className="sharebar__btn js-social-share" data-social="whatsapp" data-url={url} data-text={message} href="whatsapp://send">
                        <i className="fab fa-whatsapp"></i>
                    </a>
                </li>

                <li>
                    <a className="sharebar__btn js-social-share" data-social="mail" data-subject={subject} data-body={message} href="mailto:">
                        <i className="fas fa-envelope"></i>
                    </a>
                </li>
            </ul>

        </section>
    )

}

export default withTranslation('translations')(SocialShare);