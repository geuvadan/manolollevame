import Cookie from 'js-cookie';
import config from '../../config'

const logOut = () => {
    Cookie.remove(config.cookieName);
    localStorage.removeItem("user id");
    localStorage.removeItem("userName")
}

  export default logOut