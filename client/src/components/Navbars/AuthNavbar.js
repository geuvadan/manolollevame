import React from 'react';
import { Link } from 'react-router-dom';
import Cookie from 'js-cookie';
import config from '../../config';
import './styles.css';

import { withTranslation } from 'react-i18next';

// reactstrap components
import {
  UncontrolledCollapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
} from 'reactstrap';

class AdminNavbar extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    value: 'en',
  };

  handleChange = (event) => {
    const lang = event.target.alt;
    let newlang = event.target.alt;
    this.setState((prevState) => ({ value: newlang }));
    Cookie.set('manolo-locale', lang);
    this.props.i18n.changeLanguage(newlang);
  };

  render() {
    return (
      <>
        <Navbar className="navbar-top navbar-horizontal navbar-light" expand="md">
          <Container className="px-4">
            <NavbarBrand to="/" tag={Link}>
              <img alt="..." src={require('assets/img/brand/logoML.png')} />
            </NavbarBrand>
            <button className="navbar-toggler" id="navbar-collapse-main">
              <span className="navbar-toggler-icon" />
            </button>
            <UncontrolledCollapse navbar toggler="#navbar-collapse-main">
              <div className="navbar-collapse-header d-md-none">
                <Row>
                  <Col className="collapse-brand" xs="6">
                    <Link to="/">
                      <img alt="..." src={require('assets/img/brand/logoML.png')} />
                    </Link>
                  </Col>
                  <Col className="collapse-close" xs="6">
                    <button className="navbar-toggler" id="navbar-collapse-main">
                      <span />
                      <span />
                    </button>
                  </Col>
                </Row>
              </div>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink className="nav-link-icon" to="/admin/index" tag={Link}>
                    <i className="ni ni-planet" />
                    <span className="nav-link-inner--text">
                      {this.props.t('AdminNavbar.dropdown_menu-dashboard')}
                    </span>
                  </NavLink>
                </NavItem>
                {Cookie.get(config.cookieName) ? (
                  <NavItem>
                    <NavLink className="nav-link-icon" to="/admin/user-profile" tag={Link}>
                      <i className="ni ni-single-02" />
                      <span className="nav-link-inner--text">
                        {this.props.t('AdminNavbar.dropdown_menu-my-profile')}
                      </span>
                    </NavLink>
                  </NavItem>
                ) : (
                  <>
                    <NavItem>
                      <NavLink className="nav-link-icon" to="/auth/register" tag={Link}>
                        <i className="ni ni-circle-08" />
                        <span className="nav-link-inner--text">
                          {this.props.t('AdminNavbar.dropdown_menu-register')}
                        </span>
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink className="nav-link-icon" to="/auth/login" tag={Link}>
                        <i className="ni ni-key-25" />
                        <span className="nav-link-inner--text">
                          {this.props.t('AdminNavbar.dropdown_menu-login')}
                        </span>
                      </NavLink>
                    </NavItem>
                  </>
                )}
                <NavItem>
                  <Link to="#" className="mr-2" onClick={this.handleChange}>
                    <img
                      src={require('assets/img/lang/flag_UK.svg')}
                      alt="en"
                      className="flag-img"
                    />
                  </Link>
                  <Link to="#" className="mr-2" onClick={this.handleChange}>
                    <img
                      src={require('assets/img/lang/flag_ES.svg')}
                      alt="es"
                      className="flag-img"
                    />
                  </Link>
                </NavItem>
              </Nav>
            </UncontrolledCollapse>
          </Container>
        </Navbar>
      </>
    );
  }
}

export default withTranslation('translations')(AdminNavbar);
