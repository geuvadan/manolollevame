import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Cookie from 'js-cookie';
import config from '../../config';
import { withTranslation } from 'react-i18next';
import { useRecoilState, useRecoilValue } from 'recoil';
import { userData } from '../../services/Atoms';
import './styles.css';

// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Navbar,
  Nav,
  Container,
  Media,
} from 'reactstrap';

function AdminNavbar({ brandText, i18n, ...props }) {
  const [userName, setUserName] = useState('');
  const [logged, setLogged] = useState('');
  const [stateI18n, setStateI18n] = useState({ value: Cookie.get('manolo.locale') || 'en' });

  useEffect(() => {
    const myUserName = localStorage.getItem('userName');
    setUserName(myUserName);

    const CookieLogged = Cookie.get(config.cookieName);
    if (CookieLogged) {
      setLogged(true);
    } else {
      setLogged(false);
    }
  }, []);

  const handleLogout = (evt) => {
    evt.preventDefault();
    localStorage.clear();
    Cookie.remove(config.cookieName);
  };

  const handleChange = (event) => {
    const lang = event.target.alt;

    setStateI18n({ value: lang });
    Cookie.set('manolo-locale', lang);
    i18n.changeLanguage(lang);
  };

  const { t } = props;

  return (
    <>
      <Navbar className="navbar-top navbar-dark" expand="lg" id="navbar-main">
        <Container fluid>
          <Link className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" to="/">
            {/*brandText*/}
          </Link>
          <Nav className="align-items-center d-none d-lg-flex" navbar>
            <Link to="#" className="mr-2" onClick={handleChange}>
              <img src={require('assets/img/lang/flag_UK.svg')} alt="en" className="flag-img" />
            </Link>
            <Link to="#" className="mr-2" onClick={handleChange}>
              <img src={require('assets/img/lang/flag_ES.svg')} alt="es" className="flag-img" />
            </Link>
            <UncontrolledDropdown nav>
              <DropdownToggle className="pr-0" nav>
                <Media className="align-items-center">
                  <span className="avatar avatar-sm rounded-circle">
                    <img alt="..." src={require('assets/img/theme/undraw_female_avatar.svg')} />
                  </span>
                  <Media className="ml-2 d-none d-md-block">
                    <span className="mb-0 text-sm font-weight-bold">
                      {userName ? userName : 'Hello'}
                    </span>
                  </Media>
                </Media>
              </DropdownToggle>
              <DropdownMenu className="dropdown-menu-arrow" right>
                <DropdownItem className="noti-title" header tag="div">
                  <h6 className="text-overflow m-0">{t('AdminNavbar.dropdown_menu-welcome')}</h6>
                </DropdownItem>
                <DropdownItem to="/admin/index" tag={Link} className={logged ? 'show' : 'd-none'}>
                  <i className="ni ni-tv-2 text-info" />
                  <span>{t('AdminNavbar.dropdown_menu-dashboard')}</span>
                </DropdownItem>
                <DropdownItem to="/profile/user" tag={Link} className={logged ? 'show' : 'd-none'}>
                  <i className="ni ni-single-02 text-info" />
                  <span>{t('AdminNavbar.dropdown_menu-my-profile')}</span>
                </DropdownItem>
                <DropdownItem
                  href={process.env.REACT_APP_API_URL}
                  onClick={handleLogout}
                  className={logged ? 'show' : 'd-none'}
                >
                  <i className="ni ni-user-run text-info" />
                  <span>{t('AdminNavbar.dropdown_menu-logout')}</span>
                </DropdownItem>
                <DropdownItem to="/auth/login" tag={Link} className={!logged ? 'show' : 'd-none'}>
                  <i className="ni ni-key-25 text-info" />
                  <span>{t('AdminNavbar.dropdown_menu-login')} </span>
                </DropdownItem>
                <DropdownItem
                  to="/auth/register"
                  tag={Link}
                  className={!logged ? 'show' : 'd-none'}
                >
                  <i className="ni ni-circle-08 text-info" />
                  <span>{t('AdminNavbar.dropdown_menu-register')} </span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
}

export default withTranslation('translations')(AdminNavbar);
