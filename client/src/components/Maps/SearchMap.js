import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { useRecoilState } from 'recoil';
import { appRoutes } from '../../services/Atoms';

import Geocode from 'react-geocode';
import { GoogleMap, Marker } from '@react-google-maps/api';

import MapForm from '../Forms/MapForm';
import { usePosition } from '../../helpers/usePosition';
import PopUp from '../utils/PopUp';
import API from '../../services/API';
import config from '../../config';
import T from 'prop-types';
import { withTranslation } from 'react-i18next';

import '../../assets/css/styles.css';

function SearchMap({ fetchRoutes, ...props }) {
  /**States for map centering */
  const { userLat, userLng } = usePosition();
  const [center, setCenter] = useState();

  /**Recoil routes handling state */
  const [routes, setRoutes] = useRecoilState(appRoutes);

  /**map inputs and data handling states */
  const [data, setData] = useState({
    city: '',
    schedule: '',
    transport: '',
    seats: 0,
    description: '',
    title: '',
  });
  const [markers, setMarkers] = useState([]);
  const [address, setAddress] = useState({ from: '', to: '' });
  const [id, setId] = useState('from');

  /**others */
  const [popup, setPopup] = useState(null);
  const history = useHistory();

  const userID = localStorage.getItem('user id');

  /**Geocode library config */
  Geocode.setApiKey(config.mapsApiKey);
  Geocode.setLanguage('es');
  Geocode.setRegion('es');
  Geocode.enableDebug(false);

  /**UseEffect centers map on last marker's position
   * or user's position (to default viewing)  */

  useEffect(() => {
    markers.length > 0
      ? setCenter(markers[0].coords)
      : userLat && userLng
      ? setCenter({ lat: userLat, lng: userLng })
      : setCenter(JSON.parse(localStorage.getItem('last location')));

    return () => {};
  }, [markers, userLat, userLng]);

  /** Marker handling:
   */
  const markerToAddress = (lat, lng, id) => {
    Geocode.fromLatLng(lat, lng).then(
      (response) => {
        const pointParsed = response.results[0].formatted_address;
        setAddress({ ...address, [id]: pointParsed });
      },
      (error) => {
        console.error(error);
      }
    );
  };

  const addMarker = (marker, destroy = false) => {
    const lat = marker.lat;
    const lng = marker.lng;

    if (markers.length >= 2) {
      return;
    }

    //if(!markers || markers.length === 0) { setId("to")}

    const coords = { lat, lng };
    setMarkers((markers) => [...markers, { id, coords }]);
    markerToAddress(lat, lng, id);
    setId('to');

    if (destroy) {
      marker.setMap(null);
    }
  };

  const deleteMarker = (markerID, clearAddress = true) => {
    const filtered = markers.filter((el) => el.id !== markerID);

    setMarkers(filtered);
    setId(markerID);

    if (clearAddress) {
      setAddress({ ...address, [markerID]: '' });
    }
  };

  const updateMarker = (markerId, e) => {
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();

    markers.map((marker) => (marker.id === markerId ? (marker.coords = e.latLng.toJSON()) : null));

    markerToAddress(lat, lng, markerId);
  };

  /** methods to handle the inputs on form */

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    // from and to handled separetely
    if (name === 'from' || name === 'to') {
      setAddress({ ...address, [name]: value });
    }

    setData({ ...data, [name]: value });
  };

  const handleCoordinates = (event) => {
    const { name, value } = event.target;

    Geocode.fromAddress(`${value}, ${data.city}`).then(
      (response) => {
        const { lat, lng } = response.results[0].geometry.location;
        const coords = { lat, lng };

        setId(name);

        deleteMarker(name, false);
        setMarkers((markers) => [...markers, { id: name, coords }]);
      },
      (error) => {
        //manejar error a través de un mensaje bajo el input ("no se ha podido establecer las coordenadas, por ej")
        console.error(error);
      }
    );
  };

  /** methods to handle the actions. User can either:
   * 1. Find similar routes
   * 2. Save your route (for logged users)
   */

  const handleSearch = async (e) => {
    e.preventDefault();

    /**Checking that the user provided tw points, from and to */
    if (markers.length !== 2) {
      setPopup({
        title: props.t('Popup.searchMap.notValid.title'),
        text: props.t('Popup.searchMap.notValid.text'),
        closeText: props.t('Popup.searchMap.notValid.closeText'),
        close: () => setPopup(false),
      });
    } else {
      const from = markers.filter((marker) => marker.id === 'from')[0].coords; //markers[0].coords;
      const to = markers.filter((marker) => marker.id === 'to')[0].coords; //markers[1].coords;

      const sendData = {
        from: {
          coordinates: [from.lng, from.lat],
          type: 'Point',
        },
        to: {
          coordinates: [to.lng, to.lat],
          type: 'Point',
        },
      };
      /**
       * gotta figure out if we need extra params like schedule for the matching.
       */
      const matchRoutes = await API.matchPoints(sendData);

      const fromCoincidences = matchRoutes.results.fromMatch;
      const toCoincidences = matchRoutes.results.toMatch;
      const intersection = fromCoincidences.filter((element) => toCoincidences.includes(element));

      /** Rules to conditionally display routes.
       * At least a first approach
       */

      if (intersection.length > 0) {
        setRoutes([...intersection]);
      } else if (toCoincidences.length > 0) {
        setRoutes([...toCoincidences]);
      } else if (fromCoincidences.length > 0) {
        setRoutes([...fromCoincidences]);
      } else {
        const allRoutes = await API.getAllRoutes();
        setRoutes([...allRoutes]);

        setPopup({
          title: props.t('Popup.searchMap.notfound.title'),
          text: props.t('Popup.searchMap.notfound.text'),
          closeText: props.t('Popup.searchMap.notfound.closeText'),
          close: () => setPopup(false),
        });
      }

      setMarkers([]);
      setId('from');
      setAddress({ from: '', to: '' });
    }
  };

  const handleSave = async (e) => {
    e.preventDefault();

    if (!userID) {
      setPopup({
        title: props.t('Popup.searchMap.save.title'),
        text: props.t('Popup.searchMap.save.text'),
        closeText: props.t('Popup.searchMap.save.closeText'),
        close: () => {
          setPopup(false);
          history.push('/auth/login');
        },
      });
    }
    /**Checking that the user provided tw points, from and to */

    if (markers.length !== 2) {
      setPopup({
        title: props.t('Popup.searchMap.notValid.title'),
        text: props.t('Popup.searchMap.notValid.text'),
        closeText: props.t('Popup.searchMap.notValid.closeText'),
        close: () => setPopup(false),
      });
    } else {
      const from = markers.filter((marker) => marker.id === 'from')[0].coords; //markers[0].coords;
      const to = markers.filter((marker) => marker.id === 'to')[0].coords; //markers[1].coords;

      const sendData = {
        from: {
          coordinates: [from.lng, from.lat],
          type: 'Point',
        },
        to: {
          coordinates: [to.lng, to.lat],
          type: 'Point',
        },
        title: data.title,
        description: data.description,
        transport: data.transport,
        city: data.city,
        seats: data.seats,
        departure: data.schedule,
        user_id: userID,
      };

      const route = await API.createRoute(sendData);

      setPopup({
        title: props.t('Popup.searchMap.success.title'),
        text: props.t('Popup.searchMap.success.text'),
        closeText: props.t('Popup.searchMap.success.closeText'),
        close: () => setPopup(false),
      });

      setMarkers([]);
      setId('from');
      setAddress({ from: '', to: '' });

      fetchRoutes();
    }
  };

  const newHandleSave = (e) => {
    e.preventDefault();
    if (!userID) {
      setPopup({
        title: props.t('Popup.searchMap.save.title'),
        text: props.t('Popup.searchMap.save.text'),
        closeText: props.t('Popup.searchMap.save.closeText'),
        close: () => {
          setPopup(false);
          history.push('/auth/login');
        },
      });
    }
    if (userID) history.push('/route/create');
  };

  return (
    <div>
      {popup ? (
        <PopUp
          show={true}
          popupTitle={popup.title}
          popupText={popup.text}
          closeText={popup.closeText}
          close={popup.close}
        />
      ) : null}
      <div className="dashboard-mapWrapper">
        <div className="map-container">
          <GoogleMap
            mapContainerStyle={{ width: '100%', height: `100%` }}
            options={{ mapTypeControl: false, streetViewControl: false }}
            center={center || { lat: 38.1834068, lng: -3.69173 }}
            zoom={12}
            onClick={(e) => addMarker(e.latLng.toJSON(), false)}
          >
            {markers
              ? markers.map((marker) => {
                  return (
                    <Marker
                      key={marker.id}
                      draggable={true}
                      editable={true}
                      clickable={true}
                      position={marker.coords}
                      onDragEnd={
                        (e) => updateMarker(marker.id, e) /*marker.coords = e.latLng.toJSON()*/
                      }
                      onClick={(e) => deleteMarker(marker.id, e)}
                      // onRadiusChanged={e => updateRadius(marker.id, e)}
                    />
                  );
                })
              : null}
          </GoogleMap>
        </div>
        <div className="map-form p-3">
          <MapForm
            handleCoordinates={handleCoordinates}
            handleInputChange={handleInputChange}
            handleSave={handleSave}
            newHandleSave={newHandleSave}
            handleSearch={handleSearch}
            data={data}
            address={address}
          />
        </div>
      </div>
    </div>
  );
}
SearchMap.propTypes = {
  fetchRoutes: T.func.isRequired,
};

export default withTranslation('translations')(SearchMap);
