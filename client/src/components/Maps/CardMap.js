/*global google*/
import React, { useState, useEffect } from "react";
import { GoogleMap, DirectionsRenderer } from '@react-google-maps/api';
 

const containerStyle = {
  width: '100%',
  height: '300px'
};

const mapOptions = {
  mapTypeControl: false,
  streetViewControl: false,
}

function Map(data){

  //console.log(data);
  const { from, to } = data;
  const [directions, setDirections] = useState();
  

  useEffect(() => {

    const directionsService = new google.maps.DirectionsService();
      directionsService.route( request, (result, status) => {
        if (status === "OK") {            
            setDirections(result);
        }
      }
    );
    
    return () => {}
  }, []);
 
  const origin = { lat: from.lat, lng: from.lng };
  const destination = { lat: to.lat, lng: to.lng };

  const request = { 
    origin: origin,
    destination: destination,
    travelMode: google.maps.TravelMode.DRIVING
  }
  
  return (
    <div>
      <GoogleMap
      mapContainerStyle={containerStyle}
      zoom={5}
      options={mapOptions}
      >
        <DirectionsRenderer
          directions={directions}
        />
      </GoogleMap>
    </div>
  );
}

export default Map;
