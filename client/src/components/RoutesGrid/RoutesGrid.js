import React from "react";


import { Row, Col } from "reactstrap";
//import T from "prop-types";

import RoutesCard from "../RoutesCard";

export default function RoutesGrid({col, routes, removeRoute, editRoute, ...props }) {
    return (
        <Row>
            {routes ? routes.map((routes) => (
                <Col xl={col ? col : '4'} key={routes._id}>
                    <RoutesCard {...routes} remove={removeRoute} edit={editRoute}/>
                </Col>
            )) : null}
        </Row>
    );
}

