import axios from 'axios';
import Cookie from 'js-cookie';
import config from '../config';


const URL = process.env.REACT_APP_API_URL;

export const api = {
  registerUser: async (dataNewUser) => {
    try {
      const newUser = await axios.post(`${URL}/users/register`, dataNewUser);
      return newUser.data;
    } catch (err) {
      return err.response.data;
    }
  },

  loginUser: async (dataLogin) => {
    try {
      const login = await axios.post(`${URL}/users/login`, dataLogin);
      //save cookie - token
      const token = login.data.token;
      dataLogin.remember
        ? Cookie.set(config.cookieName, token, { expires: 30 })
        : Cookie.set(config.cookieName, token);
      localStorage.setItem('user id', login.data.user._id);
      localStorage.setItem('userName', login.data.user.username);
      return true;
    } catch (err) {
      return err.response.data;
    }
  },

  getUser: async (username) => {
    try {
      
      const user = await axios.get(`${URL}/users/${username}`);
      return user.data.result;
    } catch (err) {
      return err.response.data;
    }
  },

  getUserById: async (id) => {
    try {

      const user = await axios.get(`${URL}/users/id/${id}`);
      return user.data.result;
    } catch (err) {
      return err.response.data;
    }
  },

  deleteUser: async (id) => {
    try {
      const token = Cookie.get(config.cookieName);
      const user = await axios.delete(`${URL}/users/delete/${id}`, {
        headers: { token },
      });
      return user.data;
    } catch (err) {
      console.dir(err);
    }
  },

  updateUser: async (id, dataUpdateUser) => {
    try {
      const token = Cookie.get(config.cookieName);
      const user = await axios.put(`${URL}/users/${id}`, dataUpdateUser, {
        headers: { token },
      });
      return user.data;
    } catch (err) {
      console.dir(err);
    }
  },

  resetPassword: async (email) => {
    try {
      await axios.post(`${URL}/users/reset`, { email });
      return true;
    } catch (err) {
      return err.response.data;
    }
  },

  getTemporaryToken: async (user, token) => {
    try {
      const reset = await axios.get(`${URL}/users/reset?user=${user}&token=${token}`);
      const newToken = reset.data.token;
      Cookie.set(config.cookieName, newToken);
      localStorage.setItem('user id', reset.data.id);
      return true;
    } catch (err) {
      return err.response.data;
    }
  },

  updatePasswordAfterReset: async (id, password) => {
    try {
      const token = Cookie.get(config.cookieName);
      const user = await axios.put(`${URL}/users/reset/${id}`, password, {
        headers: { token },
      });
      return user.data;
    } catch (err) {
      console.dir(err);
    }
  },

  getRoute:
    async (id) => {
      try {
        const routes = await axios.get(`${URL}/routes/${id}`);
        return routes.data.result;

      } catch (err) {
        return err.response.data
      }
    },


  getAllRoutes: async (userId) => {
    try {
      const routes = await axios.get(`${URL}/routes`, {
        params: { userId }
      });
      return routes.data;
    } catch (err) {
      return err.response.data;
    }
  },

  getRouteById: async (id) => {
    try {
      const route = await axios.get(`${URL}/routes/${id}`);
      return route.data;
    } catch (err) {
      return err.response.data;
    }
  },

  createRoute: async (data) => {
    try {
      
      const route = await axios.post(`${URL}/routes/create`, data)
      return route.data; 

    } catch (err) {
      return err.response.data;
    }
  },

  updateRoute: async (id, dataUpdateRoute) => {
    try {
      const token = Cookie.get(config.cookieName);
      const route = await axios.put(`${URL}/routes/${id}`, dataUpdateRoute, {
        headers: { token },
      });
      return route.data;
    } catch (err) {
      console.dir(err);
    }
  },

  updateRoutesInUser: async (data, operation) => {
    try {
      const token = Cookie.get(config.cookieName);
      const route = await axios.put(`${URL}/users/${operation}/routes`, data, {
        headers: { token },
      });
      return route.data;
    } catch (err) {
      console.dir(err);
    }
  },

  deleteRoute: async(id) => {
    try {
      const token = Cookie.get(config.cookieName);
      const route = await axios.delete(`${URL}/routes/${id}`, {
        headers: { token },
      });

      return route.data;

    } catch (err) {
      console.dir(err);
    }
  },

  matchPoints: async (data) => {
    try {
      // data must include "from" point and "to" point on lng-lat format
      // It can include a "distance" parameter (expressed in meters), estating the distance to find routes within
      // If distance is not provided, It will default to 1000
      const route = await axios.post(`${URL}/routes/match`, data)

      return route.data; 
      
    } catch (err) {
      return err.response.data;
    }
  },

  userLike: async (id) => {
    try {

      const token = Cookie.get(config.cookieName);
      const route = await axios.put(`${URL}/users/like/${id}`, { user_id: id }, {
        headers: { token },
      });

      return route.data;

    } catch (err) {
      console.dir(err);
    }
  },

  sendMail: async (data) => {
    try{
      const token = Cookie.get(config.cookieName);
      //data must include 'message', 'subject' and 'email'
      const mail = await axios.post(`${URL}/users/mailing/custom`, data, {
      headers: { token },
    });

    return mail

    } catch (err) {
      return err
    }
  }
};

export default api;
