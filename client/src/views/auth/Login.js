import React, { useState } from 'react';
import API from '../../services/API';
import { withTranslation } from 'react-i18next';

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from 'reactstrap';

function Login({ ...props }) {
  const [data, setData] = useState({ email: '', password: '' });
  const [fail, setFail] = useState('');

  const handleChange = (e) => {
    e.preventDefault();
    setFail('');
    const value = e.target.value;
    const name = e.target.name;
    setData({ ...data, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const logged = await API.loginUser(data);
    if (logged === true) {
      props.history.push('/admin/index');
    } else {
      setFail({ error: logged.error, description: logged.description });
    }
  };

  return (
    <>
      <Col className="d-flex justify-content-start align-items-end" lg="6" md="8">
        <img
          alt="Register"
          className="mb-4 mb-md-0"
          style={{ width: '95%' }}
          src={require('assets/img/theme/undraw_fast_car.svg')}
        />
      </Col>
      <Col lg="6" md="8">
        <Card className="bg-secondary shadow border-1">
          <CardBody className="px-lg-5 py-lg-5">
            {fail ? (
              <div className={`alert alert-warning`} role="alert">
                {fail.description}
              </div>
            ) : (
              <div className={`text-muted alert  alert-secondary`} role="alert">
                {props.t('Login.txt')}
              </div>
            )}
            <Form role="form" onSubmit={handleSubmit}>
              <FormGroup className="mb-3">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-email-83" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="email"
                    placeholder={props.t('Login.email')}
                    type="email"
                    autoComplete="new-email"
                    onChange={handleChange}
                    value={data.email}
                  />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="password"
                    placeholder={props.t('Login.password')}
                    type="password"
                    autoComplete="new-password"
                    onChange={handleChange}
                    value={data.password}
                  />
                </InputGroup>
              </FormGroup>
              <div className="custom-control custom-control-alternative custom-checkbox">
                <input
                  className="custom-control-input"
                  id=" customCheckLogin"
                  type="checkbox"
                  name="remember"
                  onChange={handleChange}
                />
                <label className="custom-control-label" htmlFor=" customCheckLogin">
                  <span className="text-muted"> {props.t('Login.remember')}</span>
                </label>
              </div>
              <div className="text-center">
                <Button className="my-4" color="primary" type="submit">
                  {props.t('Login.sigIn')}
                </Button>
                <br />
                <Row>
                  <Col xs="6" className="text-left">
                    <a className="text-info" href="/reset/changepassword">
                      <small> {props.t('Login.forgotPassword')}</small>
                    </a>
                  </Col>
                  <Col className="text-right" xs="6">
                    <a className="text-info" href="/auth/register">
                      <small> {props.t('Login.newAccount')}</small>
                    </a>
                  </Col>
                </Row>
              </div>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </>
  );
}

export default withTranslation('translations')(Login);
