import React, { useState, useEffect } from 'react';
import API from '../../services/API';
import { useLocation } from "react-router-dom";
import PopUp from "../../components/utils/PopUp";
// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup, Form,
  Input, InputGroupAddon, InputGroupText, InputGroup,
  Col
} from "reactstrap";

function ConfirmPassword ({...props}) {

const [password, setPassword] = useState({newPassword:"", confirmPassword:""});
const [fail, setFail] = useState("");
const [popup, setPopup] = useState(false);
const queryURL = useQuery();
const [token, setToken] = useState(false);



const handleChange = e => {
  e.preventDefault();
  setFail("");
  const value = e.target.value;
  const name = e.target.name;
  setPassword({...password, [name]: value})
}

const handleSubmit = async (e) => {
  e.preventDefault();
  const id = localStorage.getItem("user id");
  if(password.newPassword === password.confirmPassword) {
     const updated = await API.updatePasswordAfterReset(id, {password: password.newPassword})
     if (updated.success === true) setPopup(true);
     if (updated.success !== true) setFail({description: "something went wrong updating your password, please try again"});
    } else {
    setFail({description: "passwords must be equal"})
  }
    
  }

const closePopup = () => {
    setPopup(false);
    props.history.push("/auth/login");
};

useEffect( () => {
    const queryUser = queryURL.get("user");
    const queryToken = queryURL.get("token");
    async function getToken(){
      const response = await API.getTemporaryToken(queryUser, queryToken);
      if (response === true) setToken(response.token);
      if (response !== true) setFail({error: response.error, description: response.description});
 
    };
    getToken();
    

},[])


  return (
      <>
        <PopUp show={popup}
               close={closePopup}
               popupTitle="new password confirmed"
               popupText="Awesome! now, let's go find amazing people to share your commuting with."
               closeText="got it!"
  />
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-1">
            <CardBody className="px-lg-5 py-lg-5">
            { fail ? 
              <div className={`alert alert-warning`} role="alert">
                {fail.description}
              </div> : 
              <div className={`text-muted alert  alert-secondary`} role="alert">
                let's change that password.
              </div> }
              <Form role="form" onSubmit={handleSubmit}>
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input name= "newPassword" placeholder="new password" type="password" onChange={handleChange} value={password.newPassword}/>
                  </InputGroup>
                </FormGroup>
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input name= "confirmPassword" placeholder="confirm password" type="password" onChange={handleChange} value={password.confirmPassword}/>
                  </InputGroup>
                </FormGroup>
                <div className="text-center">
                  <Button className="my-4" color="primary" type="submit" >
                    Send
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </>
    );
  }

  function useQuery() {
    return new URLSearchParams(useLocation().search);
  }

export default ConfirmPassword;