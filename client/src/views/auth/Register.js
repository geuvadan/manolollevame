import React, { useState } from 'react';
import API from '../../services/API';
import { Redirect, Link } from "react-router-dom";

import { withTranslation } from 'react-i18next';


// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";

function Register({ t, i18n }) {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [resRegister, setRegister] = useState('');

  const handlerRegister = async (e) => {
    e.preventDefault();
    const newUser = {
      username,
      email,
      password
    }

    const resRegister = await API.registerUser(newUser)
    resRegister.success === false ?
      setRegister(resRegister.error) :
      setRegister(resRegister.success)
  };

  if (resRegister === true) { return <Redirect exact to="/auth/login" /> }

  return (
    <>
      <Col className="d-flex justify-content-start align-items-end" lg="6" md="8">
        <img alt="Register" className="mb-4 mb-md-0" style={{ "width": "95%" }} src={require("assets/img/theme/undraw_fast_car.svg")} />
      </Col>
      <Col lg="6" md="8">
        <Card className="bg-secondary shadow border-1">
          <CardHeader className="bg-transparent">
            <h2 className="display-4 text-center text-primary"> {t("Register.title")}</h2>
          </CardHeader>
          <CardBody className="px-lg-5 py-lg-3">
            {resRegister ?
              <div className={`alert alert-warning`} role="alert">
                {resRegister}
              </div>
              : <div className={`text-muted alert  alert-secondary`} role="alert">
                {t("Register.instruction")}
              </div>
            }

            <Form onSubmit={handlerRegister} role="form">
              <FormGroup>
                <InputGroup className="input-group-alternative mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-hat-3" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder={t("Register.name")}
                    type="text"
                    required
                    onChange={e => setUsername(e.target.value)}
                    value={username}
                  />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-email-83" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder={t("Register.email")}
                    type="email"
                    autoComplete="new-email"
                    onChange={e => setEmail(e.target.value)}
                    value={email}
                    required />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder={t("Register.password")}
                    type="password"
                    onChange={e => setPassword(e.target.value)}
                    value={password}
                    required
                    autoComplete="new-password" />
                </InputGroup>
              </FormGroup>
              {/* TODO add show strong password */}
              {/* <div className="text-muted font-italic">
                <small>
                  password strength:{" "}
                  <span className="text-teal font-weight-700">strong</span>
                </small>
              </div> */}
              <Row className="my-4">
                <Col xs="12">
                  <div className="custom-control custom-control-alternative custom-checkbox">
                    <input
                      className="custom-control-input"
                      id="customCheckRegister"
                      type="checkbox"
                      required
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheckRegister"
                    >
                      <span className="text-muted">
                        {t("Register.agree")}
                        <Link to="/info/TermsConditions"> {t("Register.termsAndConditios")}</Link>
                      </span>
                    </label>
                  </div>
                </Col>
              </Row>
              <div className="text-center">
                <Button className="my-3 ml-4 btn btn-outline-info" color="primary" onClick={() => { setRegister(true) }}>
                  {t("Register.btn.later")}
                </Button>
                <Button className="my-3" color="primary" type="submit">
                  {t("Register.btn.createAccount")}
                </Button>
              </div>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </>
  );
}

export default (withTranslation("translations")(Register));

