import React, { useState } from 'react';
import API from '../../services/API';
import PopUp from "../../components/utils/PopUp";
// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup, Form,
  Input, InputGroupAddon, InputGroupText, InputGroup,
  Row,Col
} from "reactstrap";

function ChangePassword ({...props}) {

const [mail, setMail] = useState("");
const [fail, setFail] = useState("");
const [popup, setPopup] = useState(false);

const handleChange = e => {
  e.preventDefault();
  setFail("");
  const value = e.target.value;
  setMail(value)
}

const handleSubmit = async (e) => {
  e.preventDefault();
  console.log(mail)
    const reseted = await API.resetPassword(mail);
    if (reseted === true) setPopup(true)
    if (reseted !== true) setFail({error: reseted.error, description: reseted.description});
  
  }

const closePopup = () => {
    setPopup(false);
    props.history.push("/auth/login");
};

/*
useEffect(()=> { 

})*/


  return (
      <>
        <PopUp show={popup}
               close={closePopup}
               popupTitle="reset email sent"
               popupText="please check your email account and follow the instructions to reset your account's password"
               closeText="got it!"
  />
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-1">
            <CardBody className="px-lg-5 py-lg-5">
            { fail ? 
              <div className={`alert alert-warning`} role="alert">
                {fail.description}
              </div> : 
              <div className={`text-muted alert  alert-secondary`} role="alert">
                type in your email and hit send.
              </div> }
              <Form role="form" onSubmit={handleSubmit}>
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input name= "email" placeholder="Email" type="email" autoComplete="new-email" onChange={handleChange} value={mail}/>
                  </InputGroup>
                </FormGroup>
                <div className="text-center">
                  <Button className="my-4" color="primary" type="submit" >
                    Send
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
          <Row className="mt-3">
            <Col xs="6">
              <a
                className="text-light"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <small>Forgot password?</small>
              </a>
            </Col>
            <Col className="text-right" xs="6">
              <a
                className="text-light"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <small>Create new account</small>
              </a>
            </Col>
          </Row>
        </Col>
      </>
    );
  }

export default ChangePassword;