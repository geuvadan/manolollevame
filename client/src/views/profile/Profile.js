import React, { useState, useEffect } from 'react';
import API from '../../services/API';
import { Redirect, Link } from "react-router-dom";
import queryString from 'query-string';
import RoutesGrid from './../../components/RoutesGrid';
import mailTemplate from '../../variables/mailTemplates';

// lenguage
import { withTranslation } from 'react-i18next';

import { useRecoilState, useRecoilValue } from 'recoil';
import { appRoutes, userData } from '../../services/Atoms';

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col
} from "reactstrap";

// core components
import UserHeader from "components/Headers/UserHeader.js";

function Profile({ location, t }) {
  const [user, setUser] = useState({});
  const [confirmPassword, setConfirmPassword] = useState('');
  const [resDeleteUser, setResDeleteUse] = useState(false);
  const [isOwner, setIsOwner] = useState('');
  const [routes, setRoutes] = useRecoilState(appRoutes);
  const recoilUserData = useRecoilValue(userData);
  const [filteredRoutes, setFilteredRoutes] = useState();

  useEffect(() => {
    const parsed = queryString.parse(location.search);
    const username = parsed.username

    const myUserName = localStorage.getItem('userName');

    if (username) {
      getUser(username);
      setIsOwner(false)
    } else {
      getUser(myUserName)
      setIsOwner(true)
    }

    if (routes.length < 1) {
      fetchData();
    }

    const manualFilter = routes.filter(el => el.user_id === recoilUserData._id);
    setFilteredRoutes(manualFilter);

  }, [])

  const fetchData = async () => {
    const res = await API.getAllRoutes();
    setRoutes(res);
  };

  const getUser = async (username) => {
    const userAPI = await API.getUser(username)
    setUser(userAPI)
  }

  const deleteAccount = async (e) => {
    e.preventDefault();
    if (user.email === confirmPassword) {
      await mailUser({email: user.email, message: mailTemplate[1].text, subject: mailTemplate[1].subject});
      const deleteUser = await API.deleteUser(user._id)
      deleteUser.success ? setResDeleteUse(true) : setResDeleteUse(deleteUser.error)
    } else {
      setResDeleteUse('invalid email')
    }
  }

  const mailUser = (email) => {
    const template = mailTemplate[1];
    API.sendMail(email, template.subject, template.text)
  }

  const updateAccount = async (e) => {
    e.preventDefault();
    await API.updateUser(user._id, user)
    getUser(user.username)
  }

  if (resDeleteUser === true) { return <Redirect exact to="/admin/index" /> }

  return (
    <>
      <UserHeader name={user.username} owner={isOwner} />
      {/* Page content */}
      <Container className="mt--7" fluid>
        <Row>
          <Col className="order-xl-2 mb-5 mb-xl-0" xl="4">
            <Card className="card-profile shadow">
              <Row className="justify-content-center">
                <Col className="order-lg-2" lg="3">
                  <div className="card-profile-image">
                    <a href="#pablo" onClick={e => e.preventDefault()}>
                      <img
                        alt="..."
                        className="rounded-circle"
                        src={require("assets/img/theme/team-4-800x800.jpg")}
                      />
                    </a>
                  </div>
                </Col>
              </Row>
              <CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                <div className="d-flex justify-content-between">
                </div>
              </CardHeader>
              <CardBody className="pt-0 pt-md-4">
                <Row>
                  <div className="col">
                    <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                      <div>
                        <span className="heading">{user.routes ? user.routes.length : 0}</span>
                        <span className="description">{t("Profile.routes")}</span>
                      </div>
                      <div>
                        <span className="heading">{user.likes ? user.likes : 0}</span>
                        <span className="description">Likes</span>
                      </div>
                    </div>
                  </div>
                </Row>
                <div className="text-center">
                  <h3>
                    {user.username}
                  </h3>
                  <div className="h5 font-weight-300">
                    <i className="ni location_pin mr-2" />
                    {user.city ? user.city : t("Profile.city")} , {user.country ? user.country : t("Profile.country")}
                  </div>
                  {user.group ?
                    <div>{user.group}</div> : ''}
                  <hr className="my-4" />
                  {user.description ?
                    <div>
                      <div><p>{user.description}</p></div>
                      <hr className="my-4" />
                    </div>
                    : ''}
                  {user.chatOpens && user.chatOpens.length > 0 ?
                    <div>
                      <h4>{t("Profile.myChats")}</h4>
                      {user.chatOpens.map((chatOpen, id) => {
                        return (
                          <p key={id}>
                            <a href={`/route/${chatOpen.routeId}`}>{chatOpen.titleRoute}</a>
                          </p>
                        )
                      })}
                    </div>
                    : ''}
                </div>
              </CardBody>
            </Card>

          </Col>
          <Col className="order-xl-1" xl="8">
            <Card className="bg-secondary shadow">
              <CardBody className="pt-0">

                <Form onSubmit={updateAccount} >
                  <div className="pt-4">
                    <Row className="align-items-center">
                      <Col xs="8">
                        <h3 className="mb-0">{t("Profile.myAccount")}</h3>
                      </Col>
                      <Col className="text-right" xs="4">
                        <Button
                          className={isOwner ? 'show' : 'fade'}
                          color="primary"
                          type="submit"
                          size="sm"
                        >
                          {t("Profile.update")}
                        </Button>
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />
                  <h6 className="heading-small text-muted mb-4">
                    {t("Profile.userInformation")}
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-username"
                          >
                            {t("Profile.username")}
                          </label>
                          <Input
                            className="form-control-alternative"
                            defaultValue={user.username}
                            id="input-username"
                            onChange={e => setUser({ ...user, username: e.target.value })}
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="6" className={isOwner ? 'show' : 'fade'}>
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            {t("Profile.emailAddress")}
                          </label>
                          <Input
                            className="form-control-alternative"
                            id="input-email"
                            defaultValue={user.email}
                            onChange={e => setUser({ ...user, email: e.target.value })}
                            type="email"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row className={isOwner ? 'show' : 'd-none'}>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-username"
                          >
                            {t("Profile.name")}
                          </label>
                          <Input
                            className="form-control-alternative"
                            defaultValue={user.name}
                            id="input-name"
                            onChange={e => setUser({ ...user, name: e.target.value })}
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="6" className={isOwner ? 'show' : 'fade'}>
                        <label
                          className="form-control-label"
                          htmlFor="input-username"
                        >
                          {t("Profile.surname")}
                        </label>
                        <Input
                          className="form-control-alternative"
                          defaultValue={user.surname}
                          id="input-surname"
                          onChange={e => setUser({ ...user, surname: e.target.value })}
                          type="text"
                        />

                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />
                  {/* Address */}
                  <h6 className="heading-small text-muted mb-4">
                    {t("Profile.contactInformation")}
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="4">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-city"
                          >
                            {t("Profile.city")}
                          </label>
                          <Input
                            className="form-control-alternative"
                            id="input-city"
                            placeholder={t("Profile.city")}
                            type="text"
                            disabled={isOwner ? false : true}
                            defaultValue={user.city}
                            onChange={e => setUser({ ...user, city: e.target.value })}
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="4">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-country"
                          >
                            {t("Profile.country")}
                          </label>
                          <Input
                            className="form-control-alternative"
                            disabled={isOwner ? false : true}
                            placeholder={t("Profile.country")}
                            id="input-country"
                            defaultValue={user.country}
                            onChange={e => setUser({ ...user, country: e.target.value })}
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="4">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-country"
                          >
                            {t("Profile.postalCode")}
                          </label>
                          <Input
                            disabled={isOwner ? false : true}
                            placeholder={t("Profile.postalCode")}
                            className="form-control-alternative"
                            id="input-postal-code"
                            defaultValue={user.postalCode}
                            onChange={e => setUser({ ...user, postalCode: e.target.value })}
                            type="number"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />
                  {/* Description */}
                  <h6 className="heading-small text-muted mb-4">{t("Profile.aboutMe")}</h6>
                  <div className="pl-lg-4">
                    <FormGroup>
                      <Input
                        disabled={isOwner ? false : true}
                        className="form-control-alternative"
                        placeholder={t("Profile.aboutMe-placeholder")}
                        rows="3"
                        type="textarea"
                        defaultValue={user.description}
                        onChange={e => setUser({ ...user, description: e.target.value })}
                      />
                    </FormGroup>
                  </div>
                </Form>

                <hr className="my-4" />
                {/* Routes */}
                <h6 className="heading-small text-muted mb-4">{t("Profile.myRoutes")}</h6>
                <div className="pl-lg-4 mb-5">
                  <Link to="/route/create"><Button color="primary">{t("Profile.createNewRoute")}</Button></Link>
                </div>
                <div className="pl-lg-4">
                  {
                    user._id ? 
                      <RoutesGrid 
                        col='6' 
                        userId={user._id} 
                        removeRoute={isOwner ? 'true' : 'false'}
                        editRoute={isOwner ? 'true' : 'false'}
                        routes={filteredRoutes} 
                      /> : false
                  }
                </div>
                <Form onSubmit={deleteAccount}
                  className={isOwner ? 'show' : 'fade'}
                >
                  <hr className="my-4" />
                  <h6 className="heading-small text-muted mb-4">{t("Profile.deleteAccount-title")}</h6>
                  <p>{t("Profile.deleteAccount-text")}</p>
                  {resDeleteUser ?
                    <div className={`alert alert-warning`} role="alert">
                      {resDeleteUser}
                    </div>
                    : <div className={`text-danger my-2`}>
                      {t("Profile.delete-check")}
                    </div>
                  }
                  <FormGroup>
                    <Row className="justify-content-center">
                      <Col>
                        <Input
                          className="form-control-alternative mb-4 mb-lg-0"
                          placeholder={t("Profile.yourEmail")}
                          type="email"
                          value={confirmPassword}
                          onChange={e => setConfirmPassword(e.target.value)}
                          required
                        />
                      </Col>
                      <Col xs="12 d-flex justify-content-center" lg="auto" >
                        <Button
                          outline
                          type="submit"
                          color="danger"
                        >
                          {t("Profile.delete")}
                        </Button>
                      </Col>
                    </Row>
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}


export default (withTranslation("translations")(Profile));

