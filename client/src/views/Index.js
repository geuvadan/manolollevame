import React, { useState, useEffect } from 'react';
import API from '../services/API';
import { useRecoilState } from 'recoil';
import { appRoutes } from '../services/Atoms';

import { Container, Pagination, PaginationItem, PaginationLink } from 'reactstrap';

import RoutesGrid from '../components/RoutesGrid';
import Header from 'components/Headers/Header.js';

function Index() {
  const [routes, setRoutes]           = useRecoilState(appRoutes);
  const [currentPage, setCurrentPage] = useState(0);

  const pageSize   = 3;
  const pagesCount = Math.ceil(routes.length / pageSize);
  const pageRoutes = routes ? routes.slice(currentPage * pageSize, (currentPage + 1) * pageSize) : null;

  const fetchData = async () => {
    const res = await API.getAllRoutes();
    setRoutes(res);
  };

  useEffect(() => {
    if (routes.length < 1) {
      fetchData();
    }
  }, []);

  const handlePageClick = (e, index) => {
    e.preventDefault();
    setCurrentPage(index);
  };

  return (
    <>
      <Header         fetchRoutes = {fetchData} />
     
      <Container      className   = "mt-7" fluid>
      <RoutesGrid     routes      = {pageRoutes} />
      <Pagination     className   = "d-flex justify-content-center">
        <PaginationItem disabled    = {currentPage <= 0}>
            <PaginationLink
              onClick = {(e) => handlePageClick(e, currentPage - 1)}
              previous
              href = "#"
            />
        </PaginationItem>
          {[...Array(pagesCount)].map((page, i) => (
            <PaginationItem active  = {i === currentPage} key={i}>
            <PaginationLink onClick = {(e) => handlePageClick(e, i)} href = "#">
                {i + 1}
              </PaginationLink>
            </PaginationItem>
          ))}

          <PaginationItem disabled = {currentPage >= pagesCount - 1}>
          <PaginationLink onClick  = {(e) => handlePageClick(e, currentPage + 1)} next href = "#" />
          </PaginationItem>
        </Pagination>
      </Container>
      
    </>
  );
}

export default Index;
