import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

import API from '../../services/API';
import Cookie from 'js-cookie';
import config from '../../config';
import CardMap from '../../components/Maps/CardMap';
import Chat from '../../views/profile/Chat';
import SocialShare from '../../components/SocialShare/SocialShare';
import PopUp from '../../components/utils/PopUp';
// reactstrap components
import { Button, Card, CardHeader, CardBody, Container, Row, Col } from 'reactstrap';
// core components

import { withTranslation } from 'react-i18next';

function RouteDetail(props) {
  const [user, setUser] = useState(false);
  const [route, setRoute] = useState(false);
  const [from, setFrom] = useState(false);
  const [to, setTo] = useState(false);
  const [showChat, setShowChat] = useState(false);
  const { id } = useParams();
  const { t } = props;
  const [logged, setLogged] = useState(false);
  const [popup, setPopup] = useState(null);

  const loadData = async (id) => {
    const resRoute = await API.getRouteById(id);
    setRoute(resRoute.result);
    setFrom(resRoute.result.from.coordinates);
    setTo(resRoute.result.to.coordinates);

    const idUser = resRoute.result.user_id;

    const resUser = await API.getUserById(idUser);
    setUser(resUser);
  };

  useEffect(() => {
    loadData(id);
    const CookieLogged = Cookie.get(config.cookieName);
    if (CookieLogged) {
      setLogged(true);
    } else {
      setLogged(false);
    }
  }, []);

  const handelChat = () => {
    const show = showChat;
    setShowChat(!show);
  };

  const handelPopup = () => {
    setPopup({
      title: t('Popup.chat.title'),
      text: t('Popup.chat.text'),
      closeText: t('Popup.chat.closeText'),
      close: () => setPopup(false)
    })
  };

  const handelLikePopup = () => {
    setPopup({
      title: t('Popup.like.title'),
      text: t('Popup.like.text'),
      closeText: t('Popup.chat.closeText'),
      close: () => setPopup(false)
    })
  };

  const handleLike = async () => {

    try {

      const likesInUser = await API.userLike(user._id);
      document.querySelector('.user-likes').innerHTML = likesInUser.result;

    } catch (err) {
      console.log(err);
    }
  }

  return (
    <>
      {popup ? <PopUp show={true}
        popupTitle={popup.title}
        popupText={popup.text}
        closeText={popup.closeText}
        close={popup.close}
      /> : null}
      <div
        className="header d-flex align-items-center"
        style={{
          minHeight: '300px',
          backgroundColor: '#4ad4cc',
        }}
      >
        <span className="mask bg-gradient-default opacity-8" />
        <Container className="d-flex align-items-center" fluid>
          <Row>
            <Col lg="12">
              <h1 className="display-3 text-white">{route.title}</h1>
            </Col>
          </Row>
        </Container>
      </div>
      <Container className="mt--6" fluid>
        <Row className="">
          <Col className="" lg="6">
            <Card className="bg-secondary shadow">
              <CardBody className="pt-0">
                <div className="pt-4">
                  <Row className="align-items-center">
                    <Col xs="12">
                      <h2 className="mb-0">
                        <i className="fas fa-map-marker-alt mr-2"></i>
                        {route.city}
                      </h2>
                    </Col>
                  </Row>
                </div>
                <hr className="my-4" />
                <div>
                  <Row>
                    <Col>
                      {from && to ? (
                        <CardMap
                          from={{ lat: from[1], lng: from[0] }}
                          to={{ lat: to[1], lng: to[0] }}
                        />
                      ) : (
                          'Loading...'
                        )}
                    </Col>
                  </Row>
                </div>
                <hr className="my-4" />
                <h6 className="heading-small text-muted mb-4">{t('RouteDetail.information')}</h6>
                <div className="pl-lg-4">
                  <Row>
                    <Col lg="4">
                      {t('RouteDetail.transport')} {route.transport}
                    </Col>
                    <Col lg="4">
                      {t('RouteDetail.seats')} {route.seats}
                    </Col>
                    <Col lg="4">
                      {t('RouteDetail.departure-time')} {route.departure}
                    </Col>
                  </Row>
                </div>
                <hr className="my-4" />

                {route.description ? (
                  <>
                    <h6 className="heading-small text-muted mb-4">
                      {t('RouteDetail.description')}{' '}
                    </h6>
                    <div className="pl-lg-4">
                      <p className="mt-0 mb-5">{route.description}</p>
                    </div>
                  </>
                ) : (
                    ''
                  )}
                <hr className="my-4" />
                <h6 className="heading-small text-muted mb-4">{t('RouteDetail.share')}</h6>
                <div className="pl-lg-4">
                  <Row>
                    <Col>
                      <SocialShare id={id} />
                    </Col>
                  </Row>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col className=" mt-7 mt-lg-0 mb-5 mb-lg-0" lg="6">
            <Card className="card-profile shadow">
              <Row className="justify-content-center">
                <Col className="order-lg-2" lg="3">
                  <div className="card-profile-image">
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <img
                        alt="user image"
                        className="rounded-circle"
                        src={require('assets/img/theme/default_user.jpg')}
                      />
                    </a>
                  </div>
                </Col>
              </Row>
              <CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                <div className="d-flex justify-content-between">
                  <Button
                    className="mr-4 px-3"
                    color="info"
                    onClick={logged ? handleLike : handelLikePopup}
                    size="sm"
                  >
                    <i className="fas fa-thumbs-up mr-1"></i>{t('RouteDetail.profile-like')}
                  </Button>
                  <Button className="float-right" color="default"
                    onClick={logged ? handelChat : handelPopup} size="sm">
                    <i className="far fa-comment-dots mr-1"></i>{t('RouteDetail.profile-chat')}
                  </Button>
                </div>
              </CardHeader>
              <CardBody className="pt-0 pt-md-4">
                <Row>
                  <div className="col">
                    <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                      <div>
                        {user.routes ? (
                          <>
                            <span className="heading">{user.routes.length}</span>
                            <span className="description">{t('RouteDetail.profile-routes')} </span>
                          </>
                        ) : (
                            ''
                          )}
                      </div>
                      <div>
                        <span className="heading user-likes">{user.likes}</span>
                        <span className="description">{t('RouteDetail.profile-likes')} </span>
                      </div>
                    </div>
                  </div>
                </Row>
                <div className="text-center">
                  <h3>{user.username}</h3>
                  <div className="h5 font-weight-300">
                    <i className="ni location_pin mr-2" />
                    {user.city ? user.city : 'City'} , {user.country ? user.country : 'Country'}
                  </div>
                  {user.group ? <div>{user.group}</div> : ''}
                  <hr className="my-4" />
                  {user.description ? (
                    <div>
                      <p>{user.description}</p>
                    </div>
                  ) : (
                      ''
                    )}
                </div>

                <div className={showChat ? 'show' : 'd-none'}>
                  {route ? <Chat route={route._id} /> : false}
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default withTranslation('translations')(RouteDetail);
