import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Geocode from "react-geocode";
import API from '../../services/API';
import config from "../../config";
import { Link } from "react-router-dom";

import { Container, Row, Col, Form, FormGroup, Label, Input, Button, Card, CardBody, Alert } from "reactstrap";

Geocode.setApiKey(config.mapsApiKey);
Geocode.setLanguage("es");
Geocode.setRegion("es");
Geocode.enableDebug(false);

// const getAddressFromLatLng = (lat, lng) => {
//     Geocode.fromLatLng(lat, lng).then(
//         response => {
//           const address = response.results[0].formatted_address;
//           return address;
//         },
//         error => {
//           console.error(error);
//         }
//       );
//   }
  
export default function EditRoute(){


  const [route, setRoute] = useState({});
  const [message, setMessage] = useState("");
  const { id } = useParams();


  const loadData = async (id) => {

    const resRoute = await API.getRouteById(id);
    setRoute(resRoute.result);
  };

  useEffect(() => {
    loadData(id);
  }, [id]);


  const handleInputChange = (event) => {

    const { name, value } = event.target;
    setRoute({ ...route, [name]: value });
  };

  const handleCoordinates = (event) => {
  
    const { name, value } = event.target;

    Geocode.fromAddress(`${value}, ${route.city}`).then(
      response => {

        const { lat, lng } = response.results[0].geometry.location;
        setRoute({ ...route, [name]: {"type": "Point", "coordinates": [lat, lng]} });
      },
      error => {
        //manejar error a través de un mensaje bajo el input ("no se ha podido establecer las coordenadas, por ej")
        console.error(error);
      }
    );
  }


  const handleEditRoute = async () => {

    try{

      const data = {...route};
      await API.updateRoute(id, data);
      setMessage(<Alert color="info">The route has been updated successfully!. <Link to={`/route/${id}`} className="alert-link">View route</Link></Alert>);
    }catch{

      setMessage(<Alert color="danger">Oops! Something went wrong, try again...</Alert>);
    }
  }

  return (
    <>
      <div
        className="header d-flex align-items-center"
        style={{
          minHeight: "300px",
          backgroundColor: "#4ad4cc"
        }}
      >
        <span className="mask bg-gradient-default opacity-8" />
        <Container className="d-flex align-items-center" fluid>
          <Row>
            <Col lg="12">
              <h1 className="display-3 text-white">{route.title}</h1>
            </Col>
          </Row>
        </Container>
      </div>
      <Container className="mt--6" fluid>
        <Row className="">
          <Col className="" xl="10">
            <Card className="bg-secondary shadow">
              <CardBody className="pt-0">

                <Form onSubmit={(event) => {
                    event.preventDefault();
                  }}
                >
                  <div className="pt-4">
                    <Row className="align-items-center">
                      <Col xs="8">
                        <h3 className="mb-0">My route</h3>
                      </Col>
                    </Row>
                  </div>

                  <hr className="my-4" />
                  <h6 className="heading-small text-muted mb-4">
                    Departure and arrival point
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <Label for="cityRoute">Departure city</Label>
                          <Input type="text" name="city" id="cityRoute" placeholder="City of route" defaultValue={route.city} onChange={handleInputChange}/>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <Label for="fromRoute">I go out of...</Label>
                          <Input type="text" name="from" id="fromRoute" placeholder="Starting point" defaultValue="" onBlur={handleCoordinates}/>
                        </FormGroup>
                      </Col>
                      <Col lg="6">
                        <FormGroup>
                          <Label for="toRoute">Came to...</Label>
                          <Input type="text" name="to" id="toRoute" placeholder="Destination point" onBlur={handleCoordinates}/>
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>

                  <hr className="my-4" />
                  <h6 className="heading-small text-muted mb-4">
                    Name and detail of route
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <Label for="titleRoute">Route name</Label>
                          <Input type="text" name="title" id="titleRoute" placeholder="Name of my route" defaultValue={route.title} onBlur={handleInputChange}/>
                        </FormGroup>
                      </Col>
                      <Col lg="6">
                        <FormGroup>
                          <Label for="descriptionRoute">Description</Label>
                          <Input type="textarea" name="description" id="descriptionRoute" placeholder="Description of my route" defaultValue={route.description} onBlur={handleInputChange}/>
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>

                  <hr className="my-4" />
                  <h6 className="heading-small text-muted mb-4">
                    More info of route
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      

                      <Col lg="4">
                        <FormGroup>
                          <Label for="transportRoute">Transport</Label>
                          <Input type="select" name="transport" id="transportRoute" placeholder="Type of transport" value={route.transport} onChange={handleInputChange}>
                            <option>select one</option>
                            <option>coche</option>
                            <option>moto</option>
                            <option>bicicleta</option>
                            <option>a caballito</option>
                            <option>bus</option>
                          </Input>
                        </FormGroup>
                      </Col>

                      <Col lg="4">
                        <FormGroup>
                          <Label for="seatsRoute">Seats</Label>
                          <Input type="select" name="seats" id="seatsRoute" placeholder="Number of free seats" value={route.seats} onChange={handleInputChange} required>
                            <option>select one</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                          </Input>
                        </FormGroup>
                      </Col>                    

                      <Col lg="4">
                        <FormGroup>
                          <Label for="departureRoute">Departure time</Label>
                          <Input type="time" name="departure" id="departureRoute" defaultValue={route.departure} onChange={handleInputChange} required/>
                        </FormGroup>
                      </Col>
                      
                    </Row>
                  </div>

                  <div className="pl-lg-4 mt-4">
                    <Button color="primary" block onClick={handleEditRoute}>Edit route!</Button>
                  </div>

                  <div className="pl-lg-4 mt-4">{message}</div>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
