import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { useRecoilState } from 'recoil';
import { appRoutes } from '../../services/Atoms';

import Geocode from 'react-geocode';
import { GoogleMap, Marker } from '@react-google-maps/api';

import CreateForm from '../../components/Forms/CreateForm';
import { usePosition } from '../../helpers/usePosition';
import PopUp from '../../components/utils/PopUp';
import API from '../../services/API';
import config from '../../config';

function RouteCreate({ ...props }) {
  /**States for map centering */
  const { userLat, userLng } = usePosition();
  const [center, setCenter] = useState();

  /**Recoil routes handling state */
  const [routes, setRoutes] = useRecoilState(appRoutes);

  /**map inputs and data handling states */
  const [data, setData] = useState({
    city: '',
    schedule: '',
    transport: '',
    seats: 0,
    description: '',
    title: '',
  });
  const [markers, setMarkers] = useState([]);
  const [address, setAddress] = useState({ from: '', to: '' });
  const [id, setId] = useState('from');

  /**others */
  const [popup, setPopup] = useState(null);
  const history = useHistory();

  const userID = localStorage.getItem('user id');
  const lang = localStorage.getItem('i18nextLng');

  /**Geocode library config */
  Geocode.setApiKey(config.mapsApiKey);
  Geocode.setLanguage('es');
  Geocode.setRegion('es');
  Geocode.enableDebug(false);

  /**UseEffect centers map on last marker's position
   * or user's position (to default viewing)  */

  useEffect(() => {
    markers.length > 0
      ? setCenter(markers[0].coords)
      : userLat && userLng
      ? setCenter({ lat: userLat, lng: userLng })
      : setCenter(JSON.parse(localStorage.getItem('last location')));
  }, [markers, userLat, userLng]);

  /** Marker handling:
   */
  const markerToAddress = (lat, lng, id) => {
    Geocode.fromLatLng(lat, lng).then(
      (response) => {

        const pointParsed = response.results[0].formatted_address;
        setAddress({ ...address, [id]: pointParsed });
      },
      (error) => {
        console.error(error);
      }
    );
  };

  const addMarker = (marker, destroy = false) => {
    const lat = marker.lat;
    const lng = marker.lng;

    if (markers.length >= 2) {
      return;
    }

    //if(!markers || markers.length === 0) { setId("to")}

    const coords = { lat, lng };
    setMarkers((markers) => [...markers, { id, coords }]);
    markerToAddress(lat, lng, id);
    setId('to');

    if (destroy) {
      marker.setMap(null);
    }
  };

  const deleteMarker = (markerID, clearAddress = true) => {
    const filtered = markers.filter((el) => el.id !== markerID);

    setMarkers(filtered);
    setId(markerID);

    if (clearAddress) {
      setAddress({ ...address, [markerID]: '' });
    }
  };

  const updateMarker = (markerId, e) => {
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();

    markers.map((marker) => (marker.id === markerId ? (marker.coords = e.latLng.toJSON()) : null));

    markerToAddress(lat, lng, markerId);
  };

  /** methods to handle the inputs on form */

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    // from and to handled separetely
    if (name === 'from' || name === 'to') {
      setAddress({ ...address, [name]: value });
    }

    setData({ ...data, [name]: value });
  };

  const handleCoordinates = (event) => {
    const { name, value } = event.target;

    Geocode.fromAddress(`${value}, ${data.city}`).then(
      (response) => {
        const { lat, lng } = response.results[0].geometry.location;
        const coords = { lat, lng };

        setId(name);

        deleteMarker(name, false);
        setMarkers((markers) => [...markers, { id: name, coords }]);
      },
      (error) => {
        //manejar error a través de un mensaje bajo el input ("no se ha podido establecer las coordenadas, por ej")
        console.error(error);
      }
    );
  };

  /** methods to handle the actions. User can either:
   * 1. Find similar routes
   * 2. Save your route (for logged users)
   */

  const handleSave = async (e) => {
    e.preventDefault();

    if (!userID) {
      setPopup({
        title:
          lang === 'en'
            ? 'Cool!! you want to save your route to start sharing and saving?'
            : '¡Genial! ¿Quieres guardar tu ruta para empezar a compartir y guardar?',
        text:
          lang === 'en'
            ? "In order to check other user's routes you have to be logged in. Just the way you wouldn't want us to share your routes with everyone, right?"
            : 'Para poder comprobar las rutas de otros usuarios tienes que estar conectado. De la misma manera que no querrías que compartiéramos tus rutas con todo el mundo, ¿verdad?',
        closeText: lang === 'en' ? "Let's go join in" : '¡Vamos! Únete',
        close: () => {
          setPopup(false);
          history.push('/auth/login');
        },
      });
    }
    /**Checking that the user provided tw points, from and to */

    if (markers.length !== 2) {
      setPopup({
        title:
          lang === 'en'
            ? "The info you've provided is not valid"
            : 'La información que has facilitado no es válida',
        text:
          lang === 'en'
            ? 'If you want to save a route, yo should provide two markers (two points on the map) In the other hand, if you want to search for similar routes within an area, please draw two area with the polygon drawing tool.'
            : 'Si quieres guardar una ruta, debes proporcionar dos marcadores (dos puntos en el mapa) Por otro lado, si quieres buscar rutas similares dentro de un área, por favor dibuja dos áreas con la herramienta de dibujo de polígonos.',
        closeText: lang === 'en' ? 'Got it!' : 'Lo tengo!',
        close: () => setPopup(false),
      });
    } else {
      const from = markers.filter((marker) => marker.id === 'from')[0].coords; //markers[0].coords;
      const to = markers.filter((marker) => marker.id === 'to')[0].coords; //markers[1].coords;

      const sendData = {
        from: {
          coordinates: [from.lng, from.lat],
          type: 'Point',
        },
        to: {
          coordinates: [to.lng, to.lat],
          type: 'Point',
        },
        title: data.title,
        description: data.description,
        transport: data.transport,
        city: data.city,
        seats: data.seats,
        departure: data.schedule,
        user_id: userID,
      };

      const newRoute = await API.createRoute(sendData);
      const savedRoute = newRoute.savedRoute;
      setPopup({
        title: 'KUDOS!',
        text:
          lang === 'en'
            ? "Your route has been perfectly saved. You can access it, modify it or erase it (please, don't) on your profile page"
            : 'Su ruta ha sido perfectamente salvada. Puedes acceder a ella, modificarla o borrarla (por favor, no lo hagas) en tu página de perfil',
        closeText: lang === 'en' ? "Let's share!" : '¡A compartir!',
        close: () => setPopup(false),
      });

      setMarkers([]);
      setId('from');
      setAddress({ from: '', to: '' });

      setRoutes([...routes, { ...savedRoute }]);

      
      const routeUserData = {
        user_id: userID,
        route_id: savedRoute._id
      }
      await API.updateRoutesInUser(routeUserData, "add");
      
      history.push(`/route/${savedRoute._id}`);
    }
    
  };

  return (
    <div>
      {popup ? (
        <PopUp
          show={true}
          popupTitle={popup.title}
          popupText={popup.text}
          closeText={popup.closeText}
          close={popup.close}
        />
      ) : null}

      <div className="map-container">
        <GoogleMap
          mapContainerStyle={{ width: '100%', height: `100%` }}
          options={{ mapTypeControl: false, streetViewControl: false }}
          center={center || { lat: 38.1834068, lng: -3.69173 }}
          zoom={12}
          onClick={(e) => addMarker(e.latLng.toJSON(), false)}
        >
          {markers
            ? markers.map((marker) => {
                return (
                  <Marker
                    key={marker.id}
                    draggable={true}
                    editable={true}
                    clickable={true}
                    position={marker.coords}
                    onDragEnd={
                      (e) => updateMarker(marker.id, e) /*marker.coords = e.latLng.toJSON()*/
                    }
                    onClick={(e) => deleteMarker(marker.id, e)}
                    // onRadiusChanged={e => updateRadius(marker.id, e)}
                  />
                );
              })
            : null}
        </GoogleMap>
      </div>
      <CreateForm
        handleCoordinates={handleCoordinates}
        handleInputChange={handleInputChange}
        handleSave={handleSave}
        data={data}
        address={address}
      />
    </div>
  );
}

export default RouteCreate;
